[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / StorageObject

# Class: StorageObject

Class for a Storage Object, a file, that is stored in a [Container](Container.md) in Swift

## Hierarchy

- [`AuthenticatedClient`](AuthenticatedClient.md)

  ↳ **`StorageObject`**

## Table of contents

### Constructors

- [constructor](StorageObject.md#constructor)

### Properties

- [#axios](StorageObject.md##axios)
- [#context](StorageObject.md##context)
- [#identity](StorageObject.md##identity)
- [#token](StorageObject.md##token)
- [bytes](StorageObject.md#bytes)
- [containerName](StorageObject.md#containername)
- [contentType](StorageObject.md#contenttype)
- [hash](StorageObject.md#hash)
- [lastModified](StorageObject.md#lastmodified)
- [name](StorageObject.md#name)
- [service](StorageObject.md#service)
- [type](StorageObject.md#type)

### Accessors

- [context](StorageObject.md#context)
- [endpoint](StorageObject.md#endpoint)

### Methods

- [copy](StorageObject.md#copy)
- [download](StorageObject.md#download)
- [getAxios](StorageObject.md#getaxios)
- [invalidateToken](StorageObject.md#invalidatetoken)
- [move](StorageObject.md#move)
- [remove](StorageObject.md#remove)
- [convertToHeader](StorageObject.md#converttoheader)
- [get](StorageObject.md#get)
- [getHeaders](StorageObject.md#getheaders)
- [getLocalFile](StorageObject.md#getlocalfile)
- [parseHeaders](StorageObject.md#parseheaders)
- [upload](StorageObject.md#upload)

## Constructors

### constructor

• **new StorageObject**(`context`, `__namedParameters`, `containerName`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) |
| `__namedParameters` | [`RawStorageObject`](../modules.md#rawstorageobject) |
| `containerName` | `string` |

#### Overrides

[AuthenticatedClient](AuthenticatedClient.md).[constructor](AuthenticatedClient.md#constructor)

#### Defined in

[storage/StorageObject.ts:55](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-55)

## Properties

### #axios

• `Private` **#axios**: `undefined` \| `AxiosInstance` \| `AxiosStatic`

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#axios](AuthenticatedClient.md##axios)

#### Defined in

[utils/AuthenticatedClient.ts:15](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-15)

___

### #context

• `Private` **#context**: [`AuthContext`](../interfaces/AuthContext.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#context](AuthenticatedClient.md##context)

#### Defined in

[utils/AuthenticatedClient.ts:13](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-13)

___

### #identity

• `Private` **#identity**: `undefined` \| [`Identity`](Identity.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#identity](AuthenticatedClient.md##identity)

#### Defined in

[utils/AuthenticatedClient.ts:16](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-16)

___

### #token

• `Private` **#token**: `undefined` \| [`Token`](Token.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#token](AuthenticatedClient.md##token)

#### Defined in

[utils/AuthenticatedClient.ts:14](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-14)

___

### bytes

• **bytes**: `number`

Size of the file in bytes.

#### Defined in

[storage/StorageObject.ts:49](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-49)

___

### containerName

• **containerName**: `string`

Name of the container where the file is located

#### Defined in

[storage/StorageObject.ts:53](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-53)

___

### contentType

• **contentType**: `string`

MIME type of the file

#### Defined in

[storage/StorageObject.ts:36](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-36)

___

### hash

• **hash**: `string`

MD5 hash of the file contents

#### Defined in

[storage/StorageObject.ts:45](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-45)

___

### lastModified

• **lastModified**: `Date`

#### Defined in

[storage/StorageObject.ts:41](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-41)

___

### name

• **name**: `string`

Filename

#### Defined in

[storage/StorageObject.ts:40](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-40)

___

### service

• **service**: [`OpenStackService`](../modules.md#openstackservice)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[service](AuthenticatedClient.md#service)

#### Defined in

[utils/AuthenticatedClient.ts:11](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-11)

___

### type

• **type**: [`InterfaceType`](../modules.md#interfacetype)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[type](AuthenticatedClient.md#type)

#### Defined in

[utils/AuthenticatedClient.ts:12](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-12)

## Accessors

### context

• `Protected` `get` **context**(): [`AuthContext`](../interfaces/AuthContext.md)

#### Returns

[`AuthContext`](../interfaces/AuthContext.md)

#### Inherited from

AuthenticatedClient.context

#### Defined in

[utils/AuthenticatedClient.ts:24](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-24)

• `Protected` `set` **context**(`ctx`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `ctx` | [`AuthContext`](../interfaces/AuthContext.md) |

#### Returns

`void`

#### Inherited from

AuthenticatedClient.context

#### Defined in

[utils/AuthenticatedClient.ts:28](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-28)

___

### endpoint

• `get` **endpoint**(): `string`

Endpoint to the file. To be appended to the url as
`https://swifturl.com:5000/ACCOUNT/CONTAINERNAME/FILENAME`

#### Returns

`string`

#### Defined in

[storage/StorageObject.ts:78](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-78)

## Methods

### copy

▸ **copy**(`toContainer`, `filename?`, `createContainer?`): `Promise`<[`StorageObject`](StorageObject.md)\>

Copies a file into another container.

**`example`**
```typescript
const myFile = await storage.getFile('my-container', 'demo-file.pdf')
const copiedFile = await myFile.copy(
  'another-container',
  'custom-filename.pdf',
  { createContainer: true } // Create the container if it doesn't exist
)
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `toContainer` | `string` \| [`Container`](Container.md) | The container to copy to |
| `filename` | `string` | The filename to use in the target container |
| `createContainer` | `Object` | If the target container should be created if it doesn't exist. |
| `createContainer.createContainer` | `undefined` \| `boolean` | - |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

The freshly copied file

#### Defined in

[storage/StorageObject.ts:305](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-305)

___

### download

▸ **download**(`isRetry?`): `Promise`<`ReadStream`\>

Downloads the file as a stream

**`todo`** TODO: Add options for other response types

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `isRetry` | `boolean` | `false` |

#### Returns

`Promise`<`ReadStream`\>

The file content as a `ReadStream`

#### Defined in

[storage/StorageObject.ts:266](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-266)

___

### getAxios

▸ **getAxios**(): `Promise`<`AxiosInstance` \| `AxiosStatic`\>

Creates an authenticated client with baseUrl set.

#### Returns

`Promise`<`AxiosInstance` \| `AxiosStatic`\>

Axios instance with baseURL and auth headers set

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[getAxios](AuthenticatedClient.md#getaxios)

#### Defined in

[utils/AuthenticatedClient.ts:40](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-40)

___

### invalidateToken

▸ **invalidateToken**(): `void`

#### Returns

`void`

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[invalidateToken](AuthenticatedClient.md#invalidatetoken)

#### Defined in

[utils/AuthenticatedClient.ts:32](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-32)

___

### move

▸ **move**(`toContainer`, `filename?`, `createContainer?`): `Promise`<[`StorageObject`](StorageObject.md)\>

Moves a file from one container to another. It first copies then deletes
to avoid data loss.

**`example`**
```typescript
const myFile = await storage.getFile('my-container', 'demo-file.pdf')
const movedFile = await myFile.move(
  'another-container',
  'custom-filename.pdf',
  { createContainer: true } // Create the container if it doesn't exist
)

console.log(movedFile.containerName) // 'another-container'
console.log(movedFile.name) // 'custom-filename.pdf'

await storage.getFile('my-container', 'demo-file.pdf') // This now throws "not found"
```

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `toContainer` | `string` \| [`Container`](Container.md) | The target container |
| `filename` | `string` | The filename to use in the target container |
| `createContainer` | `Object` |  |
| `createContainer.createContainer` | `undefined` \| `boolean` | - |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

The copied file

#### Defined in

[storage/StorageObject.ts:342](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-342)

___

### remove

▸ **remove**(): `Promise`<`void`\>

Removes the file

#### Returns

`Promise`<`void`\>

#### Defined in

[storage/StorageObject.ts:253](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-253)

___

### convertToHeader

▸ `Static` `Private` **convertToHeader**(`__namedParameters`): [`string`, `string`]

Converts metadata to a header for passing to Swift.

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`string`, `string`] |

#### Returns

[`string`, `string`]

Key-Value pair with header name and header value

#### Defined in

[storage/StorageObject.ts:101](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-101)

___

### get

▸ `Static` **get**(`context`, `container`, `filename`, `isRetry?`): `Promise`<[`StorageObject`](StorageObject.md)\>

Fetches a single file.

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) | `undefined` | Auth contect with credentials etc |
| `container` | `string` \| [`Container`](Container.md) | `undefined` | The container where the file is located |
| `filename` | `string` | `undefined` | Name of the file |
| `isRetry` | `boolean` | `false` | - |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

Metadata about the file

#### Defined in

[storage/StorageObject.ts:155](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-155)

___

### getHeaders

▸ `Static` `Private` **getHeaders**(`axios`, `contentType`, `metadata?`): `Record`<`string`, `string`\>

Generates headers for a file upload.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `axios` | `AxiosInstance` \| `AxiosStatic` | The authenticated axios instance. Used to not override the preset headers |
| `contentType` | `string` | The content type to create |
| `metadata?` | `Record`<`string`, `string`\> | Optional metadata to assign to the file |

#### Returns

`Record`<`string`, `string`\>

The headers to add to the upload request

#### Defined in

[storage/StorageObject.ts:112](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-112)

___

### getLocalFile

▸ `Static` `Private` **getLocalFile**(`path`): [`FileData`](../modules.md#filedata)

Fetches the file contents and information about the file type.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` | Path to the file on disk |

#### Returns

[`FileData`](../modules.md#filedata)

The file content as a stream and the content type

#### Defined in

[storage/StorageObject.ts:87](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-87)

___

### parseHeaders

▸ `Static` **parseHeaders**(`headers`, `name`): [`RawStorageObject`](../modules.md#rawstorageobject)

Parses the headers from the Swift api to be able to construct a StorageObject

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `headers` | [`StorageObjectHeaders`](../modules.md#storageobjectheaders) | The headers to parse |
| `name` | `string` | Name of the file |

#### Returns

[`RawStorageObject`](../modules.md#rawstorageobject)

Raw storage objedt to pass to the [StorageObject](StorageObject.md) constructor

#### Defined in

[storage/StorageObject.ts:138](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-138)

___

### upload

▸ `Static` **upload**(`context`, `file`, `container`, `filename`, `args?`, `attempt?`): `Promise`<[`StorageObject`](StorageObject.md)\>

Uploads a file into a container.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) | Auth context with credentials etc |
| `file` | `string` \| [`FileData`](../modules.md#filedata) | Path to the file or a stream/buffer with the file content and the content type |
| `container` | `string` \| [`Container`](Container.md) | The container or it's name |
| `filename` | `string` | Name to give the file in the container. Does *not* have to be the same as the original name |
| `args?` | `Partial`<[`StorageObjectCreationArgs`](../modules.md#storageobjectcreationargs)\> | Optional arguments for the file upload such as metadata to be added or if the container should be created if it doesnt exist. |
| `attempt?` | `number` | - |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

The uploaded file

#### Defined in

[storage/StorageObject.ts:198](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-198)
