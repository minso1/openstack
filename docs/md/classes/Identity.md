[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / Identity

# Class: Identity

Class for working with the Keystone API

## Table of contents

### Constructors

- [constructor](Identity.md#constructor)

### Properties

- [#axios](Identity.md##axios)
- [#context](Identity.md##context)
- [serviceCatalog](Identity.md#servicecatalog)
- [token](Identity.md#token)

### Accessors

- [authBody](Identity.md#authbody)
- [authType](Identity.md#authtype)
- [baseUrl](Identity.md#baseurl)
- [defaultAxiosSettings](Identity.md#defaultaxiossettings)
- [endpoint](Identity.md#endpoint)
- [passwordAuth](Identity.md#passwordauth)
- [project](Identity.md#project)
- [projectScope](Identity.md#projectscope)

### Methods

- [createServiceCatalog](Identity.md#createservicecatalog)
- [getServiceCatalog](Identity.md#getservicecatalog)
- [getToken](Identity.md#gettoken)
- [loadToken](Identity.md#loadtoken)

## Constructors

### constructor

• **new Identity**(`context`, `__namedParameters?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) |
| `__namedParameters` | `Object` |
| `__namedParameters.axios` | `undefined` \| `AxiosStatic` |

#### Defined in

[identity/Identity.ts:71](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-71)

## Properties

### #axios

• `Private` **#axios**: `AxiosStatic`

#### Defined in

[identity/Identity.ts:54](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-54)

___

### #context

• `Private` **#context**: [`AuthContext`](../interfaces/AuthContext.md)

#### Defined in

[identity/Identity.ts:53](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-53)

___

### serviceCatalog

▪ `Static` **serviceCatalog**: `undefined` \| [`ServiceCatalog`](../modules.md#servicecatalog)

URLs to the services available in the Openstack implementation you are linked to.

Most services also have multiple interfaces with different use cases
such as internal communication, admin endpoints as well as public endpoints.

Used for setting base URLs to avoid having to type out the full URL every time.

#### Defined in

[identity/Identity.ts:69](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-69)

___

### token

▪ `Static` **token**: `undefined` \| [`Token`](Token.md)

The token used for authentication.

#### Defined in

[identity/Identity.ts:59](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-59)

## Accessors

### authBody

• `Private` `get` **authBody**(): [`AuthBody`](../interfaces/AuthBody.md)

The request body JSON

#### Returns

[`AuthBody`](../interfaces/AuthBody.md)

#### Defined in

[identity/Identity.ts:142](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-142)

___

### authType

• `get` **authType**(): [`AuthType`](../modules.md#authtype)

The selected authentication type

#### Returns

[`AuthType`](../modules.md#authtype)

#### Defined in

[identity/Identity.ts:105](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-105)

___

### baseUrl

• `get` **baseUrl**(): `string`

The Base URL for Keystone

#### Returns

`string`

#### Defined in

[identity/Identity.ts:90](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-90)

___

### defaultAxiosSettings

• `Private` `get` **defaultAxiosSettings**(): [`AxiosSettings`](../modules.md#axiossettings)

The default endpoint & headers for the auth calls.
Can be overriden in each method that uses it.

#### Returns

[`AxiosSettings`](../modules.md#axiossettings)

#### Defined in

[identity/Identity.ts:80](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-80)

___

### endpoint

• `get` **endpoint**(): `string`

The URL endpoint for auth

#### Returns

`string`

#### Defined in

[identity/Identity.ts:98](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-98)

___

### passwordAuth

• `Private` `get` **passwordAuth**(): [`PasswordAuthentication`](../modules.md#passwordauthentication)

The object to pass along for password authentication

#### Returns

[`PasswordAuthentication`](../modules.md#passwordauthentication)

#### Defined in

[identity/Identity.ts:119](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-119)

___

### project

• `get` **project**(): `string`

The project specified in context

#### Returns

`string`

#### Defined in

[identity/Identity.ts:112](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-112)

___

### projectScope

• `Private` `get` **projectScope**(): [`Scope`](../modules.md#scope)

The scope to use for authentication

#### Returns

[`Scope`](../modules.md#scope)

#### Defined in

[identity/Identity.ts:134](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-134)

## Methods

### createServiceCatalog

▸ `Private` **createServiceCatalog**(`data`): [`ServiceCatalog`](../modules.md#servicecatalog)

Extracts the service catalog URLs from the raw response

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `data` | [`AuthResponse`](../interfaces/AuthResponse.md) | The data to extract the catalog from |

#### Returns

[`ServiceCatalog`](../modules.md#servicecatalog)

The generated service catalog

#### Defined in

[identity/Identity.ts:193](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-193)

___

### getServiceCatalog

▸ **getServiceCatalog**(`axiosSettings?`): `Promise`<[`ServiceCatalog`](../modules.md#servicecatalog)\>

Fetches the service catalog with the different URLs provided.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `axiosSettings?` | `Partial`<[`AxiosSettings`](../modules.md#axiossettings)\> | Settings regarding endpoints & headers |

#### Returns

`Promise`<[`ServiceCatalog`](../modules.md#servicecatalog)\>

The service catalogs with the different URLs

#### Defined in

[identity/Identity.ts:208](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-208)

___

### getToken

▸ **getToken**(`axiosSettings?`): `Promise`<[`Token`](Token.md)\>

Fetches the auth token and stores it in the instance to avoid fetching
when there already is an existing and valid token.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `axiosSettings?` | `Partial`<[`AxiosSettings`](../modules.md#axiossettings)\> | Settings regarding endpoints & headers |

#### Returns

`Promise`<[`Token`](Token.md)\>

An auth token

#### Defined in

[identity/Identity.ts:228](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-228)

___

### loadToken

▸ `Private` **loadToken**(`endpoint`, `headers`, `axios?`): `Promise`<`void`\>

Fetches an auth token from the auth endpoint (Keystone) in Openstack.
It sets the `#token` && `serviceCatalog` properties on the instance.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `endpoint` | `string` | The auth endpoint in Openstack |
| `headers` | `Record`<`string`, `string`\> | The headers to pass along |
| `axios` | `Object` | Injected axios instance for testability |
| `axios.axios` | `undefined` \| `AxiosStatic` | - |

#### Returns

`Promise`<`void`\>

#### Defined in

[identity/Identity.ts:174](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-174)
