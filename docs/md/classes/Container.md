[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / Container

# Class: Container

Container that consists of 0 or more [StorageObject](StorageObject.md)

## Hierarchy

- [`AuthenticatedClient`](AuthenticatedClient.md)

  ↳ **`Container`**

## Table of contents

### Constructors

- [constructor](Container.md#constructor)

### Properties

- [#axios](Container.md##axios)
- [#context](Container.md##context)
- [#identity](Container.md##identity)
- [#token](Container.md##token)
- [bytes](Container.md#bytes)
- [content](Container.md#content)
- [count](Container.md#count)
- [lastModified](Container.md#lastmodified)
- [name](Container.md#name)
- [service](Container.md#service)
- [type](Container.md#type)

### Accessors

- [context](Container.md#context)

### Methods

- [assignStorageObjects](Container.md#assignstorageobjects)
- [getAxios](Container.md#getaxios)
- [invalidateToken](Container.md#invalidatetoken)
- [listFiles](Container.md#listfiles)
- [refresh](Container.md#refresh)
- [remove](Container.md#remove)
- [removeFiles](Container.md#removefiles)
- [uploadFile](Container.md#uploadfile)
- [convertToHeader](Container.md#converttoheader)
- [create](Container.md#create)
- [parseGetHeaders](Container.md#parsegetheaders)

## Constructors

### constructor

• **new Container**(`context`, `__namedParameters`, `content?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) |
| `__namedParameters` | [`RawContainer`](../modules.md#rawcontainer) |
| `content?` | [`RawStorageObject`](../modules.md#rawstorageobject)[] |

#### Overrides

[AuthenticatedClient](AuthenticatedClient.md).[constructor](AuthenticatedClient.md#constructor)

#### Defined in

[storage/Container.ts:33](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-33)

## Properties

### #axios

• `Private` **#axios**: `undefined` \| `AxiosInstance` \| `AxiosStatic`

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#axios](AuthenticatedClient.md##axios)

#### Defined in

[utils/AuthenticatedClient.ts:15](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-15)

___

### #context

• `Private` **#context**: [`AuthContext`](../interfaces/AuthContext.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#context](AuthenticatedClient.md##context)

#### Defined in

[utils/AuthenticatedClient.ts:13](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-13)

___

### #identity

• `Private` **#identity**: `undefined` \| [`Identity`](Identity.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#identity](AuthenticatedClient.md##identity)

#### Defined in

[utils/AuthenticatedClient.ts:16](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-16)

___

### #token

• `Private` **#token**: `undefined` \| [`Token`](Token.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#token](AuthenticatedClient.md##token)

#### Defined in

[utils/AuthenticatedClient.ts:14](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-14)

___

### bytes

• **bytes**: `number`

Total size of the container

#### Defined in

[storage/Container.ts:26](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-26)

___

### content

• **content**: `undefined` \| [`StorageObject`](StorageObject.md)[]

The content of the container.

#### Defined in

[storage/Container.ts:31](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-31)

___

### count

• **count**: `number`

Number of files in the container

#### Defined in

[storage/Container.ts:21](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-21)

___

### lastModified

• **lastModified**: `Date`

#### Defined in

[storage/Container.ts:22](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-22)

___

### name

• **name**: `string`

#### Defined in

[storage/Container.ts:27](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-27)

___

### service

• **service**: [`OpenStackService`](../modules.md#openstackservice)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[service](AuthenticatedClient.md#service)

#### Defined in

[utils/AuthenticatedClient.ts:11](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-11)

___

### type

• **type**: [`InterfaceType`](../modules.md#interfacetype)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[type](AuthenticatedClient.md#type)

#### Defined in

[utils/AuthenticatedClient.ts:12](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-12)

## Accessors

### context

• `Protected` `get` **context**(): [`AuthContext`](../interfaces/AuthContext.md)

#### Returns

[`AuthContext`](../interfaces/AuthContext.md)

#### Inherited from

AuthenticatedClient.context

#### Defined in

[utils/AuthenticatedClient.ts:24](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-24)

• `Protected` `set` **context**(`ctx`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `ctx` | [`AuthContext`](../interfaces/AuthContext.md) |

#### Returns

`void`

#### Inherited from

AuthenticatedClient.context

#### Defined in

[utils/AuthenticatedClient.ts:28](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-28)

## Methods

### assignStorageObjects

▸ `Private` **assignStorageObjects**(`content`): `void`

Converts the raw storage data to proper StorageObject and assigns it to the `content` array.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `content` | [`RawStorageObject`](../modules.md#rawstorageobject)[] | The raw storage object data |

#### Returns

`void`

#### Defined in

[storage/Container.ts:54](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-54)

___

### getAxios

▸ **getAxios**(): `Promise`<`AxiosInstance` \| `AxiosStatic`\>

Creates an authenticated client with baseUrl set.

#### Returns

`Promise`<`AxiosInstance` \| `AxiosStatic`\>

Axios instance with baseURL and auth headers set

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[getAxios](AuthenticatedClient.md#getaxios)

#### Defined in

[utils/AuthenticatedClient.ts:40](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-40)

___

### invalidateToken

▸ **invalidateToken**(): `void`

#### Returns

`void`

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[invalidateToken](AuthenticatedClient.md#invalidatetoken)

#### Defined in

[utils/AuthenticatedClient.ts:32](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-32)

___

### listFiles

▸ **listFiles**(`forceRefresh?`): `Promise`<[`StorageObject`](StorageObject.md)[]\>

Fetches the containers content.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `forceRefresh` | `Object` | if you want to make sure to get fresh data |
| `forceRefresh.forceRefresh` | `undefined` \| `boolean` | - |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)[]\>

The containers content

#### Defined in

[storage/Container.ts:91](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-91)

___

### refresh

▸ **refresh**(): `Promise`<[`Container`](Container.md)\>

Refreshes the containers content & metadata to make sure that the information is fresh af.

Useful after modfiying content and stuff like that.

#### Returns

`Promise`<[`Container`](Container.md)\>

The refreshed container

#### Defined in

[storage/Container.ts:145](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-145)

___

### remove

▸ **remove**(): `Promise`<`void`\>

Removes the container

#### Returns

`Promise`<`void`\>

#### Defined in

[storage/Container.ts:124](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-124)

___

### removeFiles

▸ **removeFiles**(`filterFunc?`): `Promise`<[`Container`](Container.md)\>

Removes file in batch from the container.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `filterFunc?` | [`FileFilterFunc`](../modules.md#filefilterfunc) | Optional function to pass to filter files arbitrarily. |

#### Returns

`Promise`<[`Container`](Container.md)\>

The modified instance

#### Defined in

[storage/Container.ts:104](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-104)

___

### uploadFile

▸ **uploadFile**(`data`, `filename`, `args?`): `Promise`<[`StorageObject`](StorageObject.md)\>

Uploads a file within the container

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `data` | `string` \| [`FileData`](../modules.md#filedata) | Path to the file you want to upload or a buffer-like object with content type provided |
| `filename` | `string` | Name of the file you want to write |
| `args?` | `Partial`<[`StorageObjectCreationArgs`](../modules.md#storageobjectcreationargs)\> | Additional arguments for uploading |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

The file created

#### Defined in

[storage/Container.ts:75](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-75)

___

### convertToHeader

▸ `Static` `Private` **convertToHeader**(`__namedParameters`): [`string`, `string`]

Converts metadata to a header for passing to Swift.

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`string`, `string`] |

#### Returns

[`string`, `string`]

Key-Value pair with header name and header value

#### Defined in

[storage/Container.ts:64](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-64)

___

### create

▸ `Static` **create**(`context`, `containerName`, `args?`): `Promise`<[`Container`](Container.md)\>

Creates (or updates if it already exists) a container.

Note that all metadata **must** be case insensitive. Both key and value.

**`see`** [Openstack Swift Documentation](https://docs.openstack.org/api-ref/object-store/index.html?expanded=show-object-metadata-detail,create-container-detail#create-container)

**`todo`** TODO: Add better support for ACL

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) | Auth context |
| `containerName` | `string` | The name of the container to create |
| `args?` | `Partial`<[`ContainerCreationArgs`](../modules.md#containercreationargs)\> | optional arguments to add metadata, permissions etc. |

#### Returns

`Promise`<[`Container`](Container.md)\>

The container that (probably) was created

#### Defined in

[storage/Container.ts:194](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-194)

___

### parseGetHeaders

▸ `Static` **parseGetHeaders**(`headers`, `name`): [`RawContainer`](../modules.md#rawcontainer)

When getting a single container the information about the container itself is in the headers.

This method parsees this into a RawContainer to be able to create a Container instance.

**`todo`** TODO: Add parsing of metadata headers

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `headers` | `any` | The headers to extract information from |
| `name` | `string` | The container name |

#### Returns

[`RawContainer`](../modules.md#rawcontainer)

The raw container to pass to the constructor

#### Defined in

[storage/Container.ts:171](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-171)
