[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / Token

# Class: Token

Authentication token class made to used for requests to Openstack

## Table of contents

### Constructors

- [constructor](Token.md#constructor)

### Properties

- [#authToken](Token.md##authtoken)
- [expiry](Token.md#expiry)

### Accessors

- [shouldRefresh](Token.md#shouldrefresh)
- [token](Token.md#token)

## Constructors

### constructor

• **new Token**(`__namedParameters`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`AuthResponse`](../interfaces/AuthResponse.md) |

#### Defined in

[identity/Token.ts:14](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Token.ts#lines-14)

## Properties

### #authToken

• `Private` **#authToken**: `string`

#### Defined in

[identity/Token.ts:7](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Token.ts#lines-7)

___

### expiry

• **expiry**: `Date`

When the token expires

#### Defined in

[identity/Token.ts:12](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Token.ts#lines-12)

## Accessors

### shouldRefresh

• `get` **shouldRefresh**(): `boolean`

Check if the time to expiry is less than 10s

#### Returns

`boolean`

#### Defined in

[identity/Token.ts:22](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Token.ts#lines-22)

___

### token

• `get` **token**(): `string`

#### Returns

`string`

#### Defined in

[identity/Token.ts:26](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Token.ts#lines-26)
