[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / FileHelper

# Class: FileHelper

## Table of contents

### Constructors

- [constructor](FileHelper.md#constructor)

### Methods

- [fileExists](FileHelper.md#fileexists)
- [fileToStream](FileHelper.md#filetostream)

## Constructors

### constructor

• **new FileHelper**()

## Methods

### fileExists

▸ `Static` **fileExists**(`path`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` |

#### Returns

`boolean`

#### Defined in

[utils/FileHelper.ts:6](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/FileHelper.ts#lines-6)

___

### fileToStream

▸ `Static` **fileToStream**(`path`): [`FileData`](../modules.md#filedata)

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` |

#### Returns

[`FileData`](../modules.md#filedata)

#### Defined in

[utils/FileHelper.ts:10](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/FileHelper.ts#lines-10)
