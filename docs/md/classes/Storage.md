[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / Storage

# Class: Storage

Wrapper class that has some helper methods for getting and setting data.

## Hierarchy

- [`AuthenticatedClient`](AuthenticatedClient.md)

  ↳ **`Storage`**

## Table of contents

### Constructors

- [constructor](Storage.md#constructor)

### Properties

- [#axios](Storage.md##axios)
- [#context](Storage.md##context)
- [#identity](Storage.md##identity)
- [#token](Storage.md##token)
- [service](Storage.md#service)
- [type](Storage.md#type)

### Accessors

- [context](Storage.md#context)

### Methods

- [createContainer](Storage.md#createcontainer)
- [getAxios](Storage.md#getaxios)
- [getContainer](Storage.md#getcontainer)
- [getContainers](Storage.md#getcontainers)
- [getFile](Storage.md#getfile)
- [invalidateToken](Storage.md#invalidatetoken)
- [removeFile](Storage.md#removefile)
- [uploadFile](Storage.md#uploadfile)

## Constructors

### constructor

• **new Storage**(`context`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `context` | [`AuthContext`](../interfaces/AuthContext.md) |

#### Overrides

[AuthenticatedClient](AuthenticatedClient.md).[constructor](AuthenticatedClient.md#constructor)

#### Defined in

[storage/Storage.ts:11](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-11)

## Properties

### #axios

• `Private` **#axios**: `undefined` \| `AxiosInstance` \| `AxiosStatic`

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#axios](AuthenticatedClient.md##axios)

#### Defined in

[utils/AuthenticatedClient.ts:15](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-15)

___

### #context

• `Private` **#context**: [`AuthContext`](../interfaces/AuthContext.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#context](AuthenticatedClient.md##context)

#### Defined in

[utils/AuthenticatedClient.ts:13](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-13)

___

### #identity

• `Private` **#identity**: `undefined` \| [`Identity`](Identity.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#identity](AuthenticatedClient.md##identity)

#### Defined in

[utils/AuthenticatedClient.ts:16](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-16)

___

### #token

• `Private` **#token**: `undefined` \| [`Token`](Token.md)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[#token](AuthenticatedClient.md##token)

#### Defined in

[utils/AuthenticatedClient.ts:14](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-14)

___

### service

• **service**: [`OpenStackService`](../modules.md#openstackservice)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[service](AuthenticatedClient.md#service)

#### Defined in

[utils/AuthenticatedClient.ts:11](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-11)

___

### type

• **type**: [`InterfaceType`](../modules.md#interfacetype)

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[type](AuthenticatedClient.md#type)

#### Defined in

[utils/AuthenticatedClient.ts:12](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-12)

## Accessors

### context

• `Protected` `get` **context**(): [`AuthContext`](../interfaces/AuthContext.md)

#### Returns

[`AuthContext`](../interfaces/AuthContext.md)

#### Inherited from

AuthenticatedClient.context

#### Defined in

[utils/AuthenticatedClient.ts:24](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-24)

• `Protected` `set` **context**(`ctx`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `ctx` | [`AuthContext`](../interfaces/AuthContext.md) |

#### Returns

`void`

#### Inherited from

AuthenticatedClient.context

#### Defined in

[utils/AuthenticatedClient.ts:28](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-28)

## Methods

### createContainer

▸ **createContainer**(`containerName`, `creationArgs?`): `Promise`<[`Container`](Container.md)\>

Creates or updates a container.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `containerName` | `string` | The name of the container to create |
| `creationArgs?` | `Partial`<[`ContainerCreationArgs`](../modules.md#containercreationargs)\> | Optional parameters for adding custom metadata and ACL etc. |

#### Returns

`Promise`<[`Container`](Container.md)\>

The container that was created

#### Defined in

[storage/Storage.ts:79](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-79)

___

### getAxios

▸ **getAxios**(): `Promise`<`AxiosInstance` \| `AxiosStatic`\>

Creates an authenticated client with baseUrl set.

#### Returns

`Promise`<`AxiosInstance` \| `AxiosStatic`\>

Axios instance with baseURL and auth headers set

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[getAxios](AuthenticatedClient.md#getaxios)

#### Defined in

[utils/AuthenticatedClient.ts:40](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-40)

___

### getContainer

▸ **getContainer**(`containerName`, `__namedParameters?`): `Promise`<[`Container`](Container.md)\>

Fetches a single storage container.

Deep, includes information about the individual objects in the container
such as size, content type, etc.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `containerName` | `string` | The container to fetch |
| `__namedParameters` | `Object` | - |
| `__namedParameters.createContainer` | `undefined` \| `boolean` | - |
| `__namedParameters.isRetry` | `undefined` \| `boolean` | - |

#### Returns

`Promise`<[`Container`](Container.md)\>

Information about the container

#### Defined in

[storage/Storage.ts:36](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-36)

___

### getContainers

▸ **getContainers**(): `Promise`<[`Container`](Container.md)[]\>

Fetches all available containers.

Shallow, no information about the individual objects in the container.

#### Returns

`Promise`<[`Container`](Container.md)[]\>

All available containers

#### Defined in

[storage/Storage.ts:21](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-21)

___

### getFile

▸ **getFile**(`container`, `filename`): `Promise`<[`StorageObject`](StorageObject.md)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `container` | `string` \| [`Container`](Container.md) |
| `filename` | `string` |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

#### Defined in

[storage/Storage.ts:86](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-86)

___

### invalidateToken

▸ **invalidateToken**(): `void`

#### Returns

`void`

#### Inherited from

[AuthenticatedClient](AuthenticatedClient.md).[invalidateToken](AuthenticatedClient.md#invalidatetoken)

#### Defined in

[utils/AuthenticatedClient.ts:32](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-32)

___

### removeFile

▸ **removeFile**(`container`, `filename`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `container` | `string` \| [`Container`](Container.md) |
| `filename` | `string` |

#### Returns

`Promise`<`void`\>

#### Defined in

[storage/Storage.ts:99](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-99)

___

### uploadFile

▸ **uploadFile**(`data`, `container`, `filename`, `args?`): `Promise`<[`StorageObject`](StorageObject.md)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `data` | `string` \| [`FileData`](../modules.md#filedata) |
| `container` | `string` \| [`Container`](Container.md) |
| `filename` | `string` |
| `args?` | `Partial`<[`StorageObjectCreationArgs`](../modules.md#storageobjectcreationargs)\> |

#### Returns

`Promise`<[`StorageObject`](StorageObject.md)\>

#### Defined in

[storage/Storage.ts:90](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Storage.ts#lines-90)
