[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / AuthenticatedClient

# Class: AuthenticatedClient

Helper class to create an authenticated client to be used as parent
for more service oriented classes.

## Hierarchy

- **`AuthenticatedClient`**

  ↳ [`Storage`](Storage.md)

  ↳ [`Container`](Container.md)

  ↳ [`StorageObject`](StorageObject.md)

## Table of contents

### Constructors

- [constructor](AuthenticatedClient.md#constructor)

### Properties

- [#axios](AuthenticatedClient.md##axios)
- [#context](AuthenticatedClient.md##context)
- [#identity](AuthenticatedClient.md##identity)
- [#token](AuthenticatedClient.md##token)
- [service](AuthenticatedClient.md#service)
- [type](AuthenticatedClient.md#type)

### Accessors

- [context](AuthenticatedClient.md#context)

### Methods

- [getAxios](AuthenticatedClient.md#getaxios)
- [invalidateToken](AuthenticatedClient.md#invalidatetoken)

## Constructors

### constructor

• **new AuthenticatedClient**(`ctx`, `service`, `type`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `ctx` | [`AuthContext`](../interfaces/AuthContext.md) |
| `service` | [`OpenStackService`](../modules.md#openstackservice) |
| `type` | [`InterfaceType`](../modules.md#interfacetype) |

#### Defined in

[utils/AuthenticatedClient.ts:18](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-18)

## Properties

### #axios

• `Private` **#axios**: `undefined` \| `AxiosInstance` \| `AxiosStatic`

#### Defined in

[utils/AuthenticatedClient.ts:15](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-15)

___

### #context

• `Private` **#context**: [`AuthContext`](../interfaces/AuthContext.md)

#### Defined in

[utils/AuthenticatedClient.ts:13](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-13)

___

### #identity

• `Private` **#identity**: `undefined` \| [`Identity`](Identity.md)

#### Defined in

[utils/AuthenticatedClient.ts:16](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-16)

___

### #token

• `Private` **#token**: `undefined` \| [`Token`](Token.md)

#### Defined in

[utils/AuthenticatedClient.ts:14](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-14)

___

### service

• **service**: [`OpenStackService`](../modules.md#openstackservice)

#### Defined in

[utils/AuthenticatedClient.ts:11](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-11)

___

### type

• **type**: [`InterfaceType`](../modules.md#interfacetype)

#### Defined in

[utils/AuthenticatedClient.ts:12](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-12)

## Accessors

### context

• `Protected` `get` **context**(): [`AuthContext`](../interfaces/AuthContext.md)

#### Returns

[`AuthContext`](../interfaces/AuthContext.md)

#### Defined in

[utils/AuthenticatedClient.ts:24](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-24)

• `Protected` `set` **context**(`ctx`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `ctx` | [`AuthContext`](../interfaces/AuthContext.md) |

#### Returns

`void`

#### Defined in

[utils/AuthenticatedClient.ts:28](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-28)

## Methods

### getAxios

▸ **getAxios**(): `Promise`<`AxiosInstance` \| `AxiosStatic`\>

Creates an authenticated client with baseUrl set.

#### Returns

`Promise`<`AxiosInstance` \| `AxiosStatic`\>

Axios instance with baseURL and auth headers set

#### Defined in

[utils/AuthenticatedClient.ts:40](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-40)

___

### invalidateToken

▸ **invalidateToken**(): `void`

#### Returns

`void`

#### Defined in

[utils/AuthenticatedClient.ts:32](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AuthenticatedClient.ts#lines-32)
