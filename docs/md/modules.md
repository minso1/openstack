[@minso_minso/openstack - v0.3.3](README.md) / Exports

# @minso_minso/openstack - v0.3.3

## Table of contents

### Classes

- [AuthenticatedClient](classes/AuthenticatedClient.md)
- [Container](classes/Container.md)
- [FileHelper](classes/FileHelper.md)
- [Identity](classes/Identity.md)
- [Storage](classes/Storage.md)
- [StorageObject](classes/StorageObject.md)
- [Token](classes/Token.md)

### Interfaces

- [ApplicationAuth](interfaces/ApplicationAuth.md)
- [AuthBody](interfaces/AuthBody.md)
- [AuthContext](interfaces/AuthContext.md)
- [AuthResponse](interfaces/AuthResponse.md)
- [UserAuth](interfaces/UserAuth.md)

### Type Aliases

- [AuthType](modules.md#authtype)
- [AxiosSettings](modules.md#axiossettings)
- [ContainerCreationArgs](modules.md#containercreationargs)
- [CreateAxiosInput](modules.md#createaxiosinput)
- [CreateAxiosOutput](modules.md#createaxiosoutput)
- [Credential](modules.md#credential)
- [Domain](modules.md#domain)
- [EndpointURLSummary](modules.md#endpointurlsummary)
- [FileData](modules.md#filedata)
- [FileFilterFunc](modules.md#filefilterfunc)
- [InterfaceType](modules.md#interfacetype)
- [OpenStackService](modules.md#openstackservice)
- [PasswordAuthentication](modules.md#passwordauthentication)
- [RawCatalog](modules.md#rawcatalog)
- [RawContainer](modules.md#rawcontainer)
- [RawEndpoint](modules.md#rawendpoint)
- [RawStorageObject](modules.md#rawstorageobject)
- [Scope](modules.md#scope)
- [ServiceCatalog](modules.md#servicecatalog)
- [StorageObjectCreationArgs](modules.md#storageobjectcreationargs)
- [StorageObjectHeaders](modules.md#storageobjectheaders)

### Variables

- [jsonFormatQuery](modules.md#jsonformatquery)

### Functions

- [createAxios](modules.md#createaxios)
- [hyphenate](modules.md#hyphenate)

## Type Aliases

### AuthType

Ƭ **AuthType**: ``"password"`` \| ``"application_credential"``

#### Defined in

[types/common.ts:81](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-81)

___

### AxiosSettings

Ƭ **AxiosSettings**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `endpoint` | `string` |
| `headers` | `Record`<`string`, `string`\> |

#### Defined in

[identity/Identity.ts:45](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-45)

___

### ContainerCreationArgs

Ƭ **ContainerCreationArgs**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `acl` | `unknown` |
| `metadata` | `Record`<`string`, `string`\> |

#### Defined in

[storage/Container.ts:9](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-9)

___

### CreateAxiosInput

Ƭ **CreateAxiosInput**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `axios` | `AxiosStatic` \| `AxiosInstance` \| `undefined` |
| `context` | [`AuthContext`](interfaces/AuthContext.md) |
| `identity` | [`Identity`](classes/Identity.md) \| `undefined` |
| `token` | [`Token`](classes/Token.md) \| `undefined` |

#### Defined in

[utils/AxiosUtils.ts:5](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AxiosUtils.ts#lines-5)

___

### CreateAxiosOutput

Ƭ **CreateAxiosOutput**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `axios` | `AxiosStatic` \| `AxiosInstance` |
| `identity` | [`Identity`](classes/Identity.md) |
| `token` | [`Token`](classes/Token.md) |

#### Defined in

[utils/AxiosUtils.ts:12](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AxiosUtils.ts#lines-12)

___

### Credential

Ƭ **Credential**: [`UserAuth`](interfaces/UserAuth.md) \| [`ApplicationAuth`](interfaces/ApplicationAuth.md)

#### Defined in

[types/common.ts:79](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-79)

___

### Domain

Ƭ **Domain**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `name` | `string` |

#### Defined in

[identity/Identity.ts:13](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-13)

___

### EndpointURLSummary

Ƭ **EndpointURLSummary**: `Record`<[`InterfaceType`](modules.md#interfacetype), `string`\>

#### Defined in

[identity/Identity.ts:41](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-41)

___

### FileData

Ƭ **FileData**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `contentType` | `string` |
| `data` | `ReadStream` \| `Buffer` |

#### Defined in

[types/common.ts:60](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-60)

___

### FileFilterFunc

Ƭ **FileFilterFunc**: (`obj`: [`StorageObject`](classes/StorageObject.md), `i`: `number`, `arr`: [`StorageObject`](classes/StorageObject.md)[]) => `boolean`

#### Type declaration

▸ (`obj`, `i`, `arr`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | [`StorageObject`](classes/StorageObject.md) |
| `i` | `number` |
| `arr` | [`StorageObject`](classes/StorageObject.md)[] |

##### Returns

`boolean`

#### Defined in

[storage/Container.ts:7](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/Container.ts#lines-7)

___

### InterfaceType

Ƭ **InterfaceType**: ``"internal"`` \| ``"public"`` \| ``"admin"``

Openstak interface types. Used to select URLs

#### Defined in

[types/common.ts:28](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-28)

___

### OpenStackService

Ƭ **OpenStackService**: ``"identity"`` \| ``"cloudformation"`` \| ``"volumev2"`` \| ``"metering"`` \| ``"placement"`` \| ``"volume"`` \| ``"load-balancer"`` \| ``"orchestration"`` \| ``"object-store"`` \| ``"key-manager"`` \| ``"rating"`` \| ``"compute"`` \| ``"network"`` \| ``"registration"`` \| ``"volumev3"`` \| ``"image"`` \| ``"metric"``

The different kind of services that are listed in the service catalog

#### Defined in

[types/common.ts:6](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-6)

___

### PasswordAuthentication

Ƭ **PasswordAuthentication**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `user` | { `domain`: [`Domain`](modules.md#domain) ; `name`: `string` ; `password`: `string`  } |
| `user.domain` | [`Domain`](modules.md#domain) |
| `user.name` | `string` |
| `user.password` | `string` |

#### Defined in

[identity/Identity.ts:15](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-15)

___

### RawCatalog

Ƭ **RawCatalog**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `endpoints` | [`RawEndpoint`](modules.md#rawendpoint)[] |
| `id` | `string` |
| `name` | `string` |
| `type` | [`OpenStackService`](modules.md#openstackservice) |

#### Defined in

[types/common.ts:53](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-53)

___

### RawContainer

Ƭ **RawContainer**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `bytes` | `number` |
| `count` | `number` |
| `last_modified` | `string` |
| `name` | `string` |

#### Defined in

[types/common.ts:38](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-38)

___

### RawEndpoint

Ƭ **RawEndpoint**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `id` | `string` |
| `interface` | [`InterfaceType`](modules.md#interfacetype) |
| `region` | `string` |
| `region_id` | `string` |
| `url` | `string` |

#### Defined in

[types/common.ts:30](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-30)

___

### RawStorageObject

Ƭ **RawStorageObject**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `bytes` | `number` |
| `content_type` | `string` |
| `hash` | `string` |
| `last_modified` | `string` |
| `name` | `string` |

#### Defined in

[types/common.ts:45](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-45)

___

### Scope

Ƭ **Scope**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `project` | { `domain`: [`Domain`](modules.md#domain) ; `name`: `string`  } |
| `project.domain` | [`Domain`](modules.md#domain) |
| `project.name` | `string` |

#### Defined in

[identity/Identity.ts:23](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-23)

___

### ServiceCatalog

Ƭ **ServiceCatalog**: `Record`<[`OpenStackService`](modules.md#openstackservice), [`EndpointURLSummary`](modules.md#endpointurlsummary)\>

#### Defined in

[identity/Identity.ts:43](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-43)

___

### StorageObjectCreationArgs

Ƭ **StorageObjectCreationArgs**: `Object`

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `createContainer` | `boolean` | If the container should be created if it does not exist. |
| `metadata` | `Record`<`string`, `string`\> | Metadata to assign to the file.  Must be case insensitive |

#### Defined in

[storage/StorageObject.ts:9](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-9)

___

### StorageObjectHeaders

Ƭ **StorageObjectHeaders**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `content-length` | `string` |
| `content-type` | `string` |
| `etag` | `string` |
| `last-modified` | `string` |

#### Defined in

[storage/StorageObject.ts:22](https://bitbucket.org/minso1/openstack/src/5a76de0/src/storage/StorageObject.ts#lines-22)

## Variables

### jsonFormatQuery

• `Const` **jsonFormatQuery**: ``"?format=json"``

#### Defined in

[utils/AxiosUtils.ts:55](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AxiosUtils.ts#lines-55)

## Functions

### createAxios

▸ **createAxios**(`input`, `service`, `type`): `Promise`<[`CreateAxiosOutput`](modules.md#createaxiosoutput)\>

Creates an authenticated axios client. To be used in [AuthenticatedClient](classes/AuthenticatedClient.md)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `input` | [`CreateAxiosInput`](modules.md#createaxiosinput) | The input to use |
| `service` | [`OpenStackService`](modules.md#openstackservice) | The service to generate token for |
| `type` | [`InterfaceType`](modules.md#interfacetype) | The type of interface to use |

#### Returns

`Promise`<[`CreateAxiosOutput`](modules.md#createaxiosoutput)\>

Authenticated client and [Identity](classes/Identity.md) instance

#### Defined in

[utils/AxiosUtils.ts:25](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/AxiosUtils.ts#lines-25)

___

### hyphenate

▸ **hyphenate**(`str`): `string`

Replaces all whitespace with hyphens.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `str` | `string` | String to hyphenate |

#### Returns

`string`

String where all whitespace are hyphens instead

#### Defined in

[utils/index.ts:10](https://bitbucket.org/minso1/openstack/src/5a76de0/src/utils/index.ts#lines-10)
