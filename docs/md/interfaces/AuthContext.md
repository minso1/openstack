[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / AuthContext

# Interface: AuthContext

## Table of contents

### Properties

- [authPort](AuthContext.md#authport)
- [authType](AuthContext.md#authtype)
- [baseUrl](AuthContext.md#baseurl)
- [credentials](AuthContext.md#credentials)
- [endpoint](AuthContext.md#endpoint)
- [project](AuthContext.md#project)

## Properties

### authPort

• **authPort**: `string` \| `number`

#### Defined in

[types/common.ts:68](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-68)

___

### authType

• **authType**: [`AuthType`](../modules.md#authtype)

#### Defined in

[types/common.ts:69](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-69)

___

### baseUrl

• **baseUrl**: `string`

#### Defined in

[types/common.ts:66](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-66)

___

### credentials

• **credentials**: [`Credential`](../modules.md#credential)

#### Defined in

[types/common.ts:70](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-70)

___

### endpoint

• `Optional` **endpoint**: `string`

#### Defined in

[types/common.ts:67](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-67)

___

### project

• **project**: `string`

#### Defined in

[types/common.ts:71](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-71)
