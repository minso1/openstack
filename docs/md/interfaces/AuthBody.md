[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / AuthBody

# Interface: AuthBody

## Table of contents

### Properties

- [auth](AuthBody.md#auth)

## Properties

### auth

• **auth**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `identity` | { `application_credential?`: [`ApplicationAuth`](ApplicationAuth.md) ; `methods`: [`AuthType`](../modules.md#authtype)[] ; `password?`: [`PasswordAuthentication`](../modules.md#passwordauthentication)  } |
| `identity.application_credential?` | [`ApplicationAuth`](ApplicationAuth.md) |
| `identity.methods` | [`AuthType`](../modules.md#authtype)[] |
| `identity.password?` | [`PasswordAuthentication`](../modules.md#passwordauthentication) |
| `scope?` | [`Scope`](../modules.md#scope) |

#### Defined in

[identity/Identity.ts:31](https://bitbucket.org/minso1/openstack/src/5a76de0/src/identity/Identity.ts#lines-31)
