[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / UserAuth

# Interface: UserAuth

## Table of contents

### Properties

- [domain](UserAuth.md#domain)
- [name](UserAuth.md#name)
- [password](UserAuth.md#password)

## Properties

### domain

• **domain**: `string`

#### Defined in

[types/common.ts:84](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-84)

___

### name

• **name**: `string`

#### Defined in

[types/common.ts:85](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-85)

___

### password

• **password**: `string`

#### Defined in

[types/common.ts:86](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-86)
