[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / AuthResponse

# Interface: AuthResponse

## Table of contents

### Properties

- [data](AuthResponse.md#data)
- [headers](AuthResponse.md#headers)

## Properties

### data

• **data**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `token` | { `catalog`: [`RawCatalog`](../modules.md#rawcatalog)[] ; `expires_at`: `string` ; `is_domain`: `boolean`  } |
| `token.catalog` | [`RawCatalog`](../modules.md#rawcatalog)[] |
| `token.expires_at` | `string` |
| `token.is_domain` | `boolean` |

#### Defined in

[types/common.ts:76](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-76)

___

### headers

• **headers**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `x-subject-token` | `string` |

#### Defined in

[types/common.ts:75](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-75)
