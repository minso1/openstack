[@minso_minso/openstack - v0.3.3](../README.md) / [Exports](../modules.md) / ApplicationAuth

# Interface: ApplicationAuth

## Table of contents

### Properties

- [id](ApplicationAuth.md#id)
- [secret](ApplicationAuth.md#secret)

## Properties

### id

• **id**: `string`

#### Defined in

[types/common.ts:90](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-90)

___

### secret

• **secret**: `string`

#### Defined in

[types/common.ts:91](https://bitbucket.org/minso1/openstack/src/5a76de0/src/types/common.ts#lines-91)
