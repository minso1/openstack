# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.3.3](https://bitbucket.org/minso1/openstack/compare/v0.3.3..v0.3.2)

### Commits

- Added possibility to pass in endpoint path in context [`91a8f84`](https://bitbucket.org/minso1/openstack/commits/91a8f847bb5d08316b83d1636fb68a136ff454e5)
- Added application credentials format to README [`a717bcf`](https://bitbucket.org/minso1/openstack/commits/a717bcf4028571c1a568a81f9af2728304c368c7)
- Test for application credentials [`e3a5072`](https://bitbucket.org/minso1/openstack/commits/e3a5072963618614510d5adec3fedded4fe5955a)
- Need to explicitly define we want responses as JSON on Binero [`d571823`](https://bitbucket.org/minso1/openstack/commits/d5718239ddcc4f507b70b1890e6ec763c0ab75ab)
- Implementing application credentials [`1c48aa3`](https://bitbucket.org/minso1/openstack/commits/1c48aa35d10f1b289d66a8ad980f55bd72095a6a)

## [v0.3.2](https://bitbucket.org/minso1/openstack/compare/v0.3.2..v0.3.1) - 2022-06-01

### Merged

- [Snyk] Upgrade axios from 0.26.0 to 0.26.1 [`#5`](https://bitbucket.org/minso1/openstack/pull-requests/5)
- [Snyk] Upgrade axios from 0.25.0 to 0.26.0 [`#4`](https://bitbucket.org/minso1/openstack/pull-requests/4)
- [Snyk] Upgrade axios from 0.24.0 to 0.25.0 [`#3`](https://bitbucket.org/minso1/openstack/pull-requests/3)

### Commits

- chore: :arrow_up: updated all deps [`cab2da9`](https://bitbucket.org/minso1/openstack/commits/cab2da907e140ab1276d0c58991c49776bf10a3e)
- fix: upgrade axios from 0.24.0 to 0.25.0 [`ac64f7e`](https://bitbucket.org/minso1/openstack/commits/ac64f7e6812f5412930e173d5f2d145b47848235)
- fix: :zap: Added retry on timeouts [`8dcb9ee`](https://bitbucket.org/minso1/openstack/commits/8dcb9eeaaa2714325ed82e7ff26fd38f7995ba57)
- fix: upgrade axios from 0.25.0 to 0.26.0 [`d509415`](https://bitbucket.org/minso1/openstack/commits/d509415ef83e189d49daf202f279a88db9d00b80)
- fix: upgrade axios from 0.26.0 to 0.26.1 [`a876b23`](https://bitbucket.org/minso1/openstack/commits/a876b23a77240dc14fd7dd0ddce168b87eb98889)

## [v0.3.1](https://bitbucket.org/minso1/openstack/compare/v0.3.1..v0.3.0) - 2021-12-29

### Merged

- [Snyk] Upgrade mime-types from 2.1.30 to 2.1.34 [`#1`](https://bitbucket.org/minso1/openstack/pull-requests/1)

### Commits

- chore: :arrow_up: upgraded all dependencies [`c5f36a9`](https://bitbucket.org/minso1/openstack/commits/c5f36a9742b849bebabf0ed7518c6e7e733fc718)
- fix: :arrow_up: fixed stuff that broke when updating deps [`2e79801`](https://bitbucket.org/minso1/openstack/commits/2e79801bcb723c02c43327026560dea6ad0eb7b8)
- fix: upgrade mime-types from 2.1.30 to 2.1.34 [`0db4bed`](https://bitbucket.org/minso1/openstack/commits/0db4bedfa8fb9970aa187af517a308dda3d4a8e7)

## [v0.3.0](https://bitbucket.org/minso1/openstack/compare/v0.3.0..v0.2.5) - 2021-11-22

### Commits

- fix: :sparkles: Added token refresh if its revoked or invalidated [`ca8d201`](https://bitbucket.org/minso1/openstack/commits/ca8d20151bcff80d5ad0c12cd3210ce83f4732e1)

## [v0.2.5](https://bitbucket.org/minso1/openstack/compare/v0.2.5..v0.2.4) - 2021-10-04

### Commits

- fix: :bug: Added url encoding so that files with special charactes can be used [`f41c965`](https://bitbucket.org/minso1/openstack/commits/f41c9659d6a90933a8dd7d8cfce37560f6a2dbb2)
- fix: Changed to infinity [`e0a0b4a`](https://bitbucket.org/minso1/openstack/commits/e0a0b4a3e923e5a56c84945b580aa9b1ea66c4bc)

## [v0.2.4](https://bitbucket.org/minso1/openstack/compare/v0.2.4..v0.2.3) - 2021-09-29

## [v0.2.3](https://bitbucket.org/minso1/openstack/compare/v0.2.3..v0.2.2) - 2021-09-29

### Commits

- updated axios [`cb75778`](https://bitbucket.org/minso1/openstack/commits/cb757789f8917d972a05916db1cbdffe4c1a9000)

## [v0.2.2](https://bitbucket.org/minso1/openstack/compare/v0.2.2..v0.2.1) - 2021-09-29

### Commits

- Changed from infinity to 1gb [`f7156e3`](https://bitbucket.org/minso1/openstack/commits/f7156e332c84287a9c1e42e8387ef1706206365b)

## [v0.2.1](https://bitbucket.org/minso1/openstack/compare/v0.2.1..v0.2.0) - 2021-09-29

### Commits

- fix: :bug: Added limits to 1gb [`5beb000`](https://bitbucket.org/minso1/openstack/commits/5beb000cd4529060da6c5a818a3a22535fccdb46)

## [v0.2.0](https://bitbucket.org/minso1/openstack/compare/v0.2.0..v0.1.10) - 2021-09-29

## [v0.1.10](https://bitbucket.org/minso1/openstack/compare/v0.1.10..v0.1.9) - 2021-09-29

### Commits

- fix: :bug: Added maxContentLength directly to the upload request [`6ae12ad`](https://bitbucket.org/minso1/openstack/commits/6ae12adbcc986ab2d4ded96423568c548a6f9fe0)

## [v0.1.9](https://bitbucket.org/minso1/openstack/compare/v0.1.9..v0.1.8) - 2021-09-29

### Commits

- fix: :bug: Fixed issue where axios threw errors for large files [`c00b2a6`](https://bitbucket.org/minso1/openstack/commits/c00b2a65c52ae906ac1bccda9b5de69a9da172f5)

## [v0.1.8](https://bitbucket.org/minso1/openstack/compare/v0.1.8..v0.1.7) - 2021-04-15

### Commits

- feat: Added copying & moving of storageobjects [`5f0b8e2`](https://bitbucket.org/minso1/openstack/commits/5f0b8e2e47fec7c424f7effe27b4c263f655bf0f)
- build: :see_no_evil: ignored env vars [`72889be`](https://bitbucket.org/minso1/openstack/commits/72889befd72aeccb17867a27acc2fccd184ee72d)

## [v0.1.7](https://bitbucket.org/minso1/openstack/compare/v0.1.7..v0.1.6) - 2021-04-15

## [v0.1.6](https://bitbucket.org/minso1/openstack/compare/v0.1.6..v0.1.5) - 2021-04-15

### Commits

- fix: :bug: Removed issue where deleting duplicates removed all files with same hash [`2e260e2`](https://bitbucket.org/minso1/openstack/commits/2e260e240ec7b0aee6a0708a6072a7b242b9e710)

## [v0.1.5](https://bitbucket.org/minso1/openstack/compare/v0.1.5..v0.1.4) - 2021-04-11

### Commits

- docs: shortened lines [`cebe680`](https://bitbucket.org/minso1/openstack/commits/cebe6808d5a6926d91a002e3b569bf38b2aebdc7)
- docs: :memo: shortened line [`1de288a`](https://bitbucket.org/minso1/openstack/commits/1de288afa711014450a8b83af9d716b6a7cf043a)

## [v0.1.4](https://bitbucket.org/minso1/openstack/compare/v0.1.4..v0.1.3) - 2021-04-11

### Commits

- docs: :page_facing_up: Added gnu gplv3 [`5f9912c`](https://bitbucket.org/minso1/openstack/commits/5f9912ce17456d570d7e440f7956e280f07e07c0)

## [v0.1.3](https://bitbucket.org/minso1/openstack/compare/v0.1.3..v0.1.2) - 2021-04-11

### Commits

- docs: :memo: Created proper documentation [`555c256`](https://bitbucket.org/minso1/openstack/commits/555c256b8b37c6dadf53699095d2ba9c1d07d2cd)

## [v0.1.2](https://bitbucket.org/minso1/openstack/compare/v0.1.2..v0.1.1) - 2021-04-11

### Commits

- docs: :memo: Switched to docs in markdown [`f9f7898`](https://bitbucket.org/minso1/openstack/commits/f9f789801999cb2333cc9f7d61acb4204a662c1b)
- docs: :memo: Added both html & md documentation [`344c5e2`](https://bitbucket.org/minso1/openstack/commits/344c5e26ae42436b9e43671a4adcbb5ba5731e9a)
- fix: :rotating_light: Fixed linter [`676483e`](https://bitbucket.org/minso1/openstack/commits/676483e43c42c5746769f667cce380403ae64023)
- build: added changelog package [`50eef7d`](https://bitbucket.org/minso1/openstack/commits/50eef7dfb769c1141b44914493366fc51b52e170)
- docs: :memo: Added docs [`9e213a4`](https://bitbucket.org/minso1/openstack/commits/9e213a42888242c5b627b3d1cd44d5fcc51055eb)
- fix: :fire: cleanup [`582713f`](https://bitbucket.org/minso1/openstack/commits/582713f5e28cd56f1478cbbf9fdd5bebba13b684)
- ci: :construction_worker: Added ci [`5224270`](https://bitbucket.org/minso1/openstack/commits/5224270ad41d33e9bf5dfeccd03e221d614960ed)
- fix: :construction_worker: switched to one step [`7e23000`](https://bitbucket.org/minso1/openstack/commits/7e23000cf48a356b4e06b6df529f6684cbd6f387)
- ci: added cache [`d566613`](https://bitbucket.org/minso1/openstack/commits/d566613346c434653ccb06b813dc2f6d8f90df43)
- ci: :bug: typo [`4cab0b8`](https://bitbucket.org/minso1/openstack/commits/4cab0b84386e3fdb3b787af604dc6ee05ef534bc)

## v0.1.1 - 2021-04-09

### Commits

- feat: :tada: Project init [`465fa1a`](https://bitbucket.org/minso1/openstack/commits/465fa1a44212393e3210fa130a023b5f0a2fe2cb)
- docs: :memo: Added documentation [`7b16d67`](https://bitbucket.org/minso1/openstack/commits/7b16d6711f72a02d842c0fd67d6e2721bddfccd2)
- feat: :sparkles: Implemented Container class [`9a10fdf`](https://bitbucket.org/minso1/openstack/commits/9a10fdff84329b17620b72738e77234d4d6a7a6b)
- feat: :sparkles: implemented StorageObject class [`1c7027c`](https://bitbucket.org/minso1/openstack/commits/1c7027c96505aa3cc79b594c7b0127b94d74a4ac)
- feat: :sparkles: Added auth module [`c8e7834`](https://bitbucket.org/minso1/openstack/commits/c8e7834b4a5902d718728633cd61dd44c63ff791)
- feat: :sparkles: Created Storage class [`83e256e`](https://bitbucket.org/minso1/openstack/commits/83e256e373640cc9a98a49610659695f7cce1d13)
- feat: :sparkles: Created storage class [`fa063ca`](https://bitbucket.org/minso1/openstack/commits/fa063ca9b878f3fb5e750477d8327120effaa1d3)
- refactor: :building_construction: Extracted axios stuff to own file [`da7a423`](https://bitbucket.org/minso1/openstack/commits/da7a4237c4e6f8078a8b3ddd0329e5c99402d4ba)
- Initial commit [`7dfd11b`](https://bitbucket.org/minso1/openstack/commits/7dfd11b953cc174c90cb48adfa7089cc3cada144)
- feat: :clown_face: stubbed container class [`edd81b8`](https://bitbucket.org/minso1/openstack/commits/edd81b84cca69bbee80a32a4e4b9bf1844d421fe)
- docs: :memo: Added doc comments [`c1a6eb9`](https://bitbucket.org/minso1/openstack/commits/c1a6eb9b79b530d34a1ac6d379187041b791df0d)
- refactor: :recycle: refactored for readability [`1d3461a`](https://bitbucket.org/minso1/openstack/commits/1d3461a26fa145b2a0fc8c4a9dd3e766a4fa67bd)
- refactor: :recycle: refactor + cleanup [`f46adda`](https://bitbucket.org/minso1/openstack/commits/f46addaee8fc76e93ca942efaf3be23e5e340252)
- fix: cleanup [`c74cf7a`](https://bitbucket.org/minso1/openstack/commits/c74cf7a9bae17a6f87f4ad5489922db13e85d935)
- fix: changed to getter & setter [`fbd43d0`](https://bitbucket.org/minso1/openstack/commits/fbd43d0f0c99af215a8ac33349d2145fe4b385ac)
- docs: :memo: Docs [`c7ccf8b`](https://bitbucket.org/minso1/openstack/commits/c7ccf8b21f1bf26a7e6146acd4b9dc6157c111a4)
- fix: :truck: Renamed to simplify export [`6162f75`](https://bitbucket.org/minso1/openstack/commits/6162f75086c9d156b3ba7d5c9ff4174aefca1394)
- feat: :clown_face: stubbed container class [`bdba34c`](https://bitbucket.org/minso1/openstack/commits/bdba34cf26206057e588e1be12e693d2fec8be12)
- test: :white_check_mark: fixed issue with static variables [`9d27676`](https://bitbucket.org/minso1/openstack/commits/9d27676b1c8652fa38524296972b68efc9cd2a6a)
- fix: :speech_balloon: removed hardcoded values [`d4f6961`](https://bitbucket.org/minso1/openstack/commits/d4f6961f3c3ff36f822c5314e8ea4b5f3dc987fe)
- fix: :truck: fixed import paths to play nice with jest [`37bab80`](https://bitbucket.org/minso1/openstack/commits/37bab8051e4016daab150ea039c36ed780343b21)
