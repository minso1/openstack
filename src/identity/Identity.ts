import _axios, { AxiosStatic } from 'axios'
import {
	ApplicationAuth,
	AuthContext,
	AuthResponse,
	AuthType,
	InterfaceType,
	OpenStackService,
	UserAuth,
} from '../types'
import { Token } from './Token'

export type Domain = { name: string }

export type PasswordAuthentication = {
	user: {
		name: string
		domain: Domain
		password: string
	}
}

export type Scope = {
	project: {
		domain: Domain
		name: string
	}
}

export interface AuthBody {
	auth: {
		identity: {
			methods: AuthType[]
			password?: PasswordAuthentication
			application_credential?: ApplicationAuth
		}
		scope?: Scope
	}
}

export type EndpointURLSummary = Record<InterfaceType, string>

export type ServiceCatalog = Record<OpenStackService, EndpointURLSummary>

export type AxiosSettings = { endpoint: string; headers: Record<string, string> }

const DEFAULT_ENDPOINT = '/v3/auth/tokens'

/**
 * Class for working with the Keystone API
 */
export class Identity {
	#context: AuthContext
	#axios: AxiosStatic

	/**
	 * The token used for authentication.
	 */
	public static token: Token | undefined

	/**
	 * URLs to the services available in the Openstack implementation you are linked to.
	 *
	 * Most services also have multiple interfaces with different use cases
	 * such as internal communication, admin endpoints as well as public endpoints.
	 *
	 * Used for setting base URLs to avoid having to type out the full URL every time.
	 */
	public static serviceCatalog: ServiceCatalog | undefined

	constructor(context: AuthContext, { axios = _axios } = {}) {
		this.#axios = axios
		this.#context = context
	}

	/**
	 * The default endpoint & headers for the auth calls.
	 * Can be overriden in each method that uses it.
	 */
	private get defaultAxiosSettings(): AxiosSettings {
		return {
			endpoint: DEFAULT_ENDPOINT,
			headers: { 'Content-Type': 'application/json' },
		}
	}

	/**
	 * The Base URL for Keystone
	 */
	get baseUrl(): string {
		const { baseUrl, authPort } = this.#context
		return `${baseUrl}:${authPort}`
	}

	/**
	 * The URL endpoint for auth
	 */
	get endpoint(): string {
		return this.#context.endpoint || DEFAULT_ENDPOINT
	}

	/**
	 * The selected authentication type
	 */
	get authType(): AuthType {
		return this.#context.authType
	}

	/**
	 * The project specified in context
	 */
	get project(): string {
		return this.#context.project
	}

	/**
	 * The object to pass along for password authentication
	 */
	private get passwordAuth(): PasswordAuthentication {
		const { domain, name, password } = this.#context.credentials as UserAuth

		return {
			user: {
				name,
				domain: { name: domain },
				password,
			},
		}
	}

	/**
	 * The scope to use for authentication
	 */
	private get projectScope(): Scope {
		const { domain } = this.#context.credentials as UserAuth
		return { project: { domain: { name: domain }, name: this.project } }
	}

	/**
	 * The request body JSON
	 */
	private get authBody(): AuthBody {
		if (this.authType === 'password')
			return {
				auth: {
					identity: {
						methods: ['password'],
						password: this.passwordAuth,
					},
					scope: this.projectScope,
				},
			} as AuthBody

		if (this.authType === 'application_credential')
			return {
				auth: {
					identity: {
						methods: ['application_credential'],
						application_credential: this.#context.credentials,
					},
				},
			} as AuthBody
		
		throw new Error('invalid auth type:' + this.authType)
	}

	/**
	 * Fetches an auth token from the auth endpoint (Keystone) in Openstack.
	 * It sets the `#token` && `serviceCatalog` properties on the instance.
	 *
	 * @param endpoint The auth endpoint in Openstack
	 * @param headers The headers to pass along
	 * @param axios Injected axios instance for testability
	 */
	private async loadToken(
		endpoint: string,
		headers: Record<string, string>,
		{ axios = this.#axios } = {}
	): Promise<void> {
		const url = this.baseUrl + endpoint
		const body = this.authBody

		const resp = await axios.post<any, AuthResponse>(url, body, { headers })

		Identity.token = new Token(resp)
		Identity.serviceCatalog = this.createServiceCatalog(resp)
	}

	/**
	 * Extracts the service catalog URLs from the raw response
	 * @param data The data to extract the catalog from
	 * @returns The generated service catalog
	 */
	private createServiceCatalog({ data }: AuthResponse): ServiceCatalog {
		return data.token.catalog.reduce((acc, service) => {
			acc[service.type] = service.endpoints.reduce((urlAcc, endpoint) => {
				urlAcc[endpoint.interface] = endpoint.url
				return urlAcc
			}, {} as EndpointURLSummary)
			return acc
		}, {} as ServiceCatalog)
	}

	/**
	 * Fetches the service catalog with the different URLs provided.
	 * @param axiosSettings Settings regarding endpoints & headers
	 * @returns The service catalogs with the different URLs
	 */
	public async getServiceCatalog(axiosSettings?: Partial<AxiosSettings>): Promise<ServiceCatalog> {
		const { endpoint, headers } = {
			...this.defaultAxiosSettings,
			...{ endpoint: this.#context.endpoint || DEFAULT_ENDPOINT },
			...(axiosSettings ?? {}),
		}

		if (!Identity.serviceCatalog) {
			await this.loadToken(endpoint, headers)
		}

		return Identity.serviceCatalog as ServiceCatalog
	}

	/**
	 * Fetches the auth token and stores it in the instance to avoid fetching
	 * when there already is an existing and valid token.
	 * @param axiosSettings Settings regarding endpoints & headers
	 * @returns An auth token
	 */
	public async getToken(axiosSettings?: Partial<AxiosSettings>): Promise<Token> {
		const { endpoint, headers } = {
			...this.defaultAxiosSettings,
			...{ endpoint: this.#context.endpoint || DEFAULT_ENDPOINT },
			...(axiosSettings ?? {}),
		}

		// Check that the token is loaded and not expired
		if (!Identity.token || Identity.token?.shouldRefresh) {
			await this.loadToken(endpoint, headers)
		}

		return Identity.token as Token
	}
}
