import { Identity } from '../Identity'

describe('Identity', () => {
	let identity: Identity, axios: any, post: any, context: any

	beforeEach(() => {
		context = {
			baseUrl: 'www.google.se',
			authType: 'password',
			authPort: 8888,
			project: 'test-project',
			credentials: {
				domain: 'default',
				name: 'username',
				password: 'supersecretpassword',
			},
		}

		post = jest.fn().mockResolvedValue({
			headers: {},
			data: {
				token: {
					expires_at: new Date(Date.now() + 100000).toISOString(),
					catalog: [],
				},
			},
		})

		axios = { post }

		identity = new Identity(context, { axios })

		Identity.token = undefined
		Identity.serviceCatalog = undefined
	})

	it('has a working constructor', () => {
		expect(identity).toBeInstanceOf(Identity)
		expect(identity.baseUrl).toBe('www.google.se:8888')
	})

	describe('Get Token', () => {
		it('should use default settings if none provided', async () => {
			await identity.getToken()
			expect(post).toBeCalledWith(
				`${context.baseUrl}:${context.authPort}/v3/auth/tokens`,
				{
					auth: {
						identity: {
							methods: ['password'],
							password: {
								user: {
									domain: { name: context.credentials.domain },
									name: context.credentials.name,
									password: context.credentials.password,
								},
							},
						},
						scope: {
							project: {
								domain: { name: context.credentials.domain },
								name: context.project,
							},
						},
					},
				},
				{ headers: { 'Content-Type': 'application/json' } }
			)
		})

		it('should use provided endpoint if set directly', async () => {
			await identity.getToken({ endpoint: '/test/v5' })
			expect(post.mock.calls[0][0]).toBe(`${context.baseUrl}:${context.authPort}/test/v5`)
		})

		it('should use provided endpoint if set in context', async () => {
			context.endpoint = '/auth/v4yo'
			await identity.getToken()
			expect(post.mock.calls[0][0]).toBe(`${context.baseUrl}:${context.authPort}/auth/v4yo`)
		})

		it('should use provided endpoint from param if set both directly and in context', async () => {
			context.endpoint = '/auth/v4yo'
			await identity.getToken({ endpoint: '/test/v5' })
			expect(post.mock.calls[0][0]).toBe(`${context.baseUrl}:${context.authPort}/test/v5`)
		})

		it('should use application credentials if provided', async () => {
			context.authType = 'application_credential'
			context.credentials = {
				id: 'appid',
				secret: 'appsecret',
			},

			await identity.getToken()
			expect(post).toBeCalledWith(
				`${context.baseUrl}:${context.authPort}/v3/auth/tokens`,
				{
					auth: {
						identity: {
							methods: ['application_credential'],
							application_credential: {
								id: 'appid',
								secret: 'appsecret'
							},
						},
					},
				},
				{ headers: { 'Content-Type': 'application/json' } }
			)
		})

		it('should store the token to avoid overfetching', async () => {
			await identity.getToken()
			await identity.getToken()

			expect(post).toBeCalledTimes(1)
		})

		it('should refresh the token if expired', async () => {
			const expires_at = new Date(Date.now() - 100000).toISOString()

			post.mockResolvedValue({
				headers: {},
				data: {
					token: {
						expires_at,
						catalog: [],
					},
				},
			})

			await identity.getToken()
			await identity.getToken()

			expect(post).toBeCalledTimes(2)
		})

		it('should propagate errors', async () => {
			expect.hasAssertions()

			const e = new Error('this went poopoo')
			post.mockRejectedValue(e)

			try {
				await identity.getToken()
			} catch (err) {
				expect(err).toBe(e)
			}
		})
	})

	describe('Get Service Catalog', () => {
		it('should use default settings if none provided', async () => {
			await identity.getServiceCatalog()
			expect(post).toBeCalledWith(
				`${context.baseUrl}:${context.authPort}/v3/auth/tokens`,
				{
					auth: {
						identity: {
							methods: ['password'],
							password: {
								user: {
									domain: { name: context.credentials.domain },
									name: context.credentials.name,
									password: context.credentials.password,
								},
							},
						},
						scope: {
							project: {
								domain: { name: context.credentials.domain },
								name: context.project,
							},
						},
					},
				},
				{ headers: { 'Content-Type': 'application/json' } }
			)
		})

		it('should use provided settings if provided', async () => {
			await identity.getServiceCatalog({ endpoint: '/test/v5' })
			expect(post.mock.calls[0][0]).toBe(`${context.baseUrl}:${context.authPort}/test/v5`)
		})

		it('should _NOT_ call to fetch the catalog if it already exist', async () => {
			await identity.getServiceCatalog()
			await identity.getServiceCatalog()

			expect(post).toBeCalledTimes(1)
		})

		it('should propagate errors', async () => {
			expect.hasAssertions()

			const e = new Error('this went poopoo')

			post.mockRejectedValue(e)

			try {
				await identity.getServiceCatalog()
			} catch (err) {
				expect(err).toBe(e)
			}
		})
	})
})
