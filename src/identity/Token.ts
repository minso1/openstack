import { AuthResponse } from '../types'

/**
 * Authentication token class made to used for requests to Openstack
 */
export class Token {
	#authToken: string

	/**
	 * When the token expires
	 */
	public expiry: Date

	constructor({ headers, data }: AuthResponse) {
		this.expiry = new Date(data.token.expires_at)
		this.#authToken = headers['x-subject-token']
	}

	/**
	 * Check if the time to expiry is less than 10s
	 */
	get shouldRefresh(): boolean {
		return this.expiry.getTime() - Date.now() < 10000
	}

	get token(): string {
		return this.#authToken
	}
}
