import { AuthContext } from '../../types'
import { jsonFormatQuery } from '../../utils'
import { Container } from '../Container'
import { StorageObject } from '../StorageObject'

const mockClientConstructor = jest.fn().mockImplementation(function (name, service, type) {
	// @ts-ignore
	this.name = name
	// @ts-ignore
	this.service = service
	// @ts-ignore
	this.type = type
})

const put = jest.fn()

const mockDelete = jest.fn()

const get = jest.fn().mockResolvedValue({
	data: [
		{
			hash: 'asdfghjk',
			bytes: 42,
			content_type: 'application/pdf',
			last_modified: '2014-01-15T16:37:43.427570',
			name: 'test.pdf',
		},
	],
	headers: {
		'x-container-bytes-used': 1337,
		'last-modified': '2014-01-15T16:37:43.427570',
		'x-container-object-count': 42,
	},
})
const mockAxios = {
	get,
	put,
	delete: mockDelete,
	defaults: {
		headers: {},
	},
}

const mockGetAxios = jest.fn().mockResolvedValue(mockAxios)

jest.mock('../../utils', () => {
	return {
		AuthenticatedClient: class AuthClientMock {
			constructor(ctx: any[], service: any, type: any) {
				return mockClientConstructor.bind(this)(ctx, service, type)
			}

			getAxios(...args: any[]) {
				return mockGetAxios(...args)
			}
		},
		hyphenate: (str: string): string => str.replace(/\s+/g, '-'),
	}
})

describe('Container', () => {
	let c: Container, ctx: AuthContext

	beforeEach(() => {
		jest.clearAllMocks()

		ctx = {
			authPort: 1234,
			authType: 'password',
			baseUrl: 'www.google.se',
			project: 'my-project',
			credentials: {
				domain: 'default',
				name: 'kalle@anka.se',
				password: 'supersecretpassword',
			},
		}

		c = new Container(ctx, {
			bytes: 0,
			count: 0,
			last_modified: '2014-01-14T16:37:43.427570',
			name: 'my-name',
		})
	})

	it('should have a working constructor', () => {
		expect(c).toBeInstanceOf(Container)
		expect(mockClientConstructor).toBeCalledTimes(1)
		expect(mockClientConstructor).toBeCalledWith(ctx, 'object-store', 'public')
	})

	describe('Create', () => {
		it.each([
			'',
			'with space',
			'with	tab',
			`with
			linebreak`,
			'asdhakdjhaadahsdjkahsdkajsdhaksdjhadskjhskjdhaskdjhaskdjhaskdjhaskdjhaksdjhaksjdhaksjdhakjhdaksdhakdshaksdjhaksdjhaksdjhaksjhdakjshdakjdhadhaskdjhasdkjhasdkjahsdkjahdkajhsdkajhdskajhsdkajhdakjshdakjsdhakjshdakjdhakjsdhakjshdakjsdhaksjhdadksjhaskdjhadkjhdakjhaksdjhasdkjhasdkajhsd',
		])('should throw with invalid container name', async (name: string) => {
			expect.hasAssertions()
			try {
				await Container.create(ctx, name)
			} catch (err: any) {
				expect(err.message).toBeTruthy()
			}
		})

		it('should create an axios instance', async () => {
			await Container.create(ctx, 'name')

			expect(mockClientConstructor).toBeCalledWith(ctx, 'object-store', 'public')
			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to create the container', async () => {
			await Container.create(ctx, 'name')

			expect(put).toBeCalledWith('/name', {}, { headers: {} })
		})

		it('should add metadata to headers if specified', async () => {
			await Container.create(ctx, 'name', { metadata: { custom: 'some custom data' } })

			expect(put).toBeCalledWith(
				'/name',
				{},
				{ headers: { 'X-Container-Meta-custom': 'some-custom-data' } }
			)
		})

		it('should keep original headers if adding new ones', async () => {
			mockGetAxios.mockResolvedValueOnce({
				...mockAxios,
				defaults: { headers: { common: { authentication: 'some auth token' } } },
			})

			await Container.create(ctx, 'name', { metadata: { custom: 'some custom data' } })

			expect(put).toBeCalledWith(
				'/name',
				{},
				{
					headers: {
						'X-Container-Meta-custom': 'some-custom-data',
						authentication: 'some auth token',
					},
				}
			)
		})

		it('should call to fetch the created container', async () => {
			await Container.create(ctx, 'name')

			expect(get).toBeCalledWith('/name' + jsonFormatQuery)
		})

		it('should return the created container', async () => {
			const resp = await Container.create(ctx, 'name')

			expect(resp.bytes).toBe(1337)
			expect(resp.count).toBe(42)
			expect(resp.name).toBe('name')
			expect(resp.content?.every((c) => c instanceof StorageObject)).toBe(true)
		})

		it('should propagate errors', async () => {
			expect.hasAssertions()

			const e = new Error('this went poopoo')

			put.mockRejectedValueOnce(e)

			try {
				await Container.create(ctx, 'name')
			} catch (err: any) {
				expect(err).toBe(e)
			}
		})
	})

	describe('Parse Headers', () => {
		let headers: any

		beforeEach(() => {
			headers = {
				'x-container-bytes-used': 1337,
				'last-modified': '2014-01-15T16:37:43.427570',
				'x-container-object-count': 42,
			}
		})

		it('should parse the wanted headers', () => {
			expect(Container.parseGetHeaders(headers, 'custom-name')).toEqual({
				bytes: 1337,
				count: 42,
				last_modified: '2014-01-15T16:37:43.427570',
				name: 'custom-name',
			})
		})

		it('should disregard all other headers', () => {
			headers.authentication = 'Bearer ahdlahsdlajdshajd'

			expect(Container.parseGetHeaders(headers, 'custom-name')).toEqual({
				bytes: 1337,
				count: 42,
				last_modified: '2014-01-15T16:37:43.427570',
				name: 'custom-name',
			})
		})

		it.todo('should keep dynamic metadata headers')
	})

	describe('Refresh', () => {
		it('should create an axios instance', async () => {
			await c.refresh()

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to get the data', async () => {
			await c.refresh()

			expect(get).toBeCalledTimes(1)
			expect(get).toBeCalledWith('/my-name' + jsonFormatQuery)
		})

		it('should assign the new data to the instance', async () => {
			expect(c.count).toBe(0)

			await c.refresh()

			expect(c.count).toBe(42)
		})
	})

	describe('Remove', () => {
		it('should get axios instance', async () => {
			await c.remove()

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to delete the container', async () => {
			await c.remove()

			expect(mockDelete).toBeCalledWith('/my-name')
		})

		it.each([404, 403])('should return simple error for %p', async (status: number) => {
			expect.hasAssertions()

			mockDelete.mockRejectedValueOnce({ response: { status } })

			try {
				await c.remove()
			} catch (err: any) {
				expect(err.message).toBe('Container does not exist or not allowed.')
			}
		})

		it('should return original error for other status codes', async () => {
			const e = { response: { status: 500 } }
			mockDelete.mockRejectedValueOnce(e)

			try {
				await c.remove()
			} catch (err: any) {
				expect(err).toBe(e)
			}
		})
	})

	describe('Remove files', () => {
		let mockFiles: any[]

		beforeEach(() => {
			mockFiles = [
				{ hash: '1', name: '1.pdf', bytes: 25, remove: jest.fn() },
				{ hash: '2', name: '2.pdf', bytes: 25, remove: jest.fn() },
				{ hash: '3', name: '3.pdf', bytes: 25, remove: jest.fn() },
				{ hash: '2', name: '4.pdf', bytes: 25, remove: jest.fn() },
			]

			c.content = mockFiles
			c.bytes = 100
			c.count = mockFiles.length
		})

		it('should remove all files if no filter', async () => {
			await c.removeFiles()

			for (const f of mockFiles) {
				expect(f.remove).toBeCalled()
			}
		})

		it('should remove filtered files if specified', async () => {
			const filterFunc = (f: any) => ['1', '3'].includes(f.hash)

			await c.removeFiles(filterFunc)

			expect(mockFiles[0].remove).toBeCalled()
			expect(mockFiles[2].remove).toBeCalled()
			expect(mockFiles[1].remove).not.toBeCalled()
			expect(mockFiles[3].remove).not.toBeCalled()
		})

		it('should updata instance metadata', async () => {
			const filterFunc = (f: any) => f.hash !== '2'

			const initial = c.lastModified.getTime()
			await c.removeFiles(filterFunc)

			expect(c.count).toBe(2)
			expect(c.bytes).toBe(50)
			expect(c.content).toEqual([mockFiles[1], mockFiles[3]])
			expect(c.lastModified.getTime()).toBeGreaterThan(initial)
		})

		it('should be able to delete duplicates of file', async () => {
			// This was an issue where it was filtering files after
			// deletion by hash. But if you wanted to remove a duplicate
			// both instances of the file was removed.

			const filterFunc = (f: any) => f.name === '2.pdf'

			await c.removeFiles(filterFunc)

			expect(c.content).toEqual([mockFiles[0], mockFiles[2], mockFiles[3]])
		})
	})

	describe('List files', () => {
		beforeEach(() => {
			c.refresh = jest.fn(c.refresh)
		})

		it('should call to fetch content if there is none', async () => {
			c.content = undefined

			await c.listFiles()

			expect((c as any).refresh).toBeCalledTimes(1)
		})

		it('should call to fetch content if forced refresh', async () => {
			c.content = []

			await c.listFiles({ forceRefresh: true })

			expect((c as any).refresh).toBeCalledTimes(1)
		})

		it('should not fetch content if it is defined', async () => {
			c.content = []

			await c.listFiles()

			expect((c as any).refresh).toBeCalledTimes(0)
		})

		it('should return the instances content', async () => {
			const resp = await c.listFiles()

			expect(resp).toBe(c.content)
			expect(resp.every((f) => f instanceof StorageObject)).toBe(true)
		})
	})
})
