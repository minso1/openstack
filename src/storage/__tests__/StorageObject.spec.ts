import { AuthContext, RawStorageObject } from '../../types'
import { StorageObject } from '../StorageObject'

const mockFileExists = jest.fn().mockReturnValue(true)

const mockFileToStream = jest.fn().mockReturnValue({
	contentType: 'application/pdf',
	data: Buffer.from([]),
})

const mockClientConstructor = jest.fn().mockImplementation(function (name, service, type) {
	// @ts-ignore
	this.name = name
	// @ts-ignore
	this.service = service
	// @ts-ignore
	this.type = type
})

const put = jest.fn()
const head = jest.fn().mockResolvedValue({
	data: '',
	headers: {
		'content-length': '123456',
		'last-modified': '2014-01-15T16:37:43.427570',
		etag: 'somemd5hash',
		'content-type': 'application/pdf',
	},
})

const mockDelete = jest.fn()

const get = jest.fn().mockResolvedValue({
	data: [],
	headers: {
		'content-length': '123456',
		'last-modified': '2014-01-15T16:37:43.427570',
		etag: 'somemd5hash',
		'content-type': 'application/pdf',
	},
})
const mockAxios = {
	get,
	put,
	head,
	delete: mockDelete,
	defaults: {
		headers: {},
	},
}

const mockGetAxios = jest.fn().mockResolvedValue(mockAxios)

jest.mock('../../utils', () => {
	return {
		AuthenticatedClient: class AuthClientMock {
			constructor(ctx: any[], service: any, type: any) {
				return mockClientConstructor.bind(this)(ctx, service, type)
			}

			getAxios(...args: any[]) {
				return mockGetAxios(...args)
			}
		},
		FileHelper: class FileHelperMock {
			static fileExists(...args: any[]) {
				return mockFileExists(...args)
			}

			static fileToStream(...args: any[]) {
				return mockFileToStream(...args)
			}
		},
		hyphenate: (str: string): string => str.replace(/\s+/g, '-'),
	}
})

describe('Storage Object', () => {
	let s: StorageObject, ctx: AuthContext, data: RawStorageObject

	beforeEach(() => {
		jest.clearAllMocks()

		data = {
			bytes: 1337,
			content_type: 'application/pdf',
			hash: 'md5hash',
			last_modified: '2014-01-15T16:37:43.427570',
			name: 'file-name.pdf',
		}

		ctx = {
			authPort: 1234,
			authType: 'password',
			baseUrl: 'www.google.se',
			project: 'my-project',
			credentials: {
				domain: 'default',
				name: 'kalle@anka.se',
				password: 'supersecretpassword',
			},
		}

		s = new StorageObject(ctx, data, 'test-container')
	})

	it('should have a working constructor', () => {
		expect(s).toBeInstanceOf(StorageObject)
		expect(mockClientConstructor).toBeCalledTimes(1)
		expect(mockClientConstructor).toBeCalledWith(ctx, 'object-store', 'public')
	})

	describe('Parse Headers', () => {
		let headers: any

		beforeEach(() => {
			headers = {
				'content-length': '123456',
				'last-modified': '2014-01-15T16:37:43.427570',
				etag: 'somemd5hash',
				'content-type': 'application/pdf',
			}
		})

		it('should parse the defined headers', () => {
			expect(StorageObject.parseHeaders(headers, 'file-name.pdf')).toEqual({
				bytes: 123456,
				content_type: 'application/pdf',
				hash: 'somemd5hash',
				last_modified: '2014-01-15T16:37:43.427570',
				name: 'file-name.pdf',
			})
		})

		it('should discard unspecified headers', () => {
			headers.Authentication = 'Bearer helloyo'

			expect(StorageObject.parseHeaders(headers, 'file-name.pdf')).toEqual({
				bytes: 123456,
				content_type: 'application/pdf',
				hash: 'somemd5hash',
				last_modified: '2014-01-15T16:37:43.427570',
				name: 'file-name.pdf',
			})
		})

		it.todo('should parse custom metadata headers')
	})

	describe('Get', () => {
		beforeEach(() => {
			jest.clearAllMocks()

			data = {
				bytes: 1337,
				content_type: 'application/pdf',
				hash: 'md5hash',
				last_modified: '2014-01-15T16:37:43.427570',
				name: 'file-name.pdf',
			}

			ctx = {
				authPort: 1234,
				authType: 'password',
				baseUrl: 'www.google.se',
				project: 'my-project',
				credentials: {
					domain: 'default',
					name: 'kalle@anka.se',
					password: 'supersecretpassword',
				},
			}

			head.mockResolvedValue({
				data: '',
				headers: {
					'content-length': '123456',
					'last-modified': '2014-01-15T16:37:43.427570',
					etag: 'somemd5hash',
					'content-type': 'application/pdf',
				},
			})

			s = new StorageObject(ctx, data, 'test-container')
		})

		it('should get authenticated axios instance', async () => {
			await StorageObject.get(ctx, 'container', 'file.pdf')

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it.each([{ name: 'container' }, 'container'])(
			'should call HEAD to the correct endpoint',
			async (container: any) => {
				await StorageObject.get(ctx, container, 'file.pdf')

				expect(head).toBeCalledWith('/container/file.pdf')
			}
		)

		it.each([
			['file.pdf', 'file.pdf'],
			['file 123.pdf', 'file%20123.pdf'],
			['bäääjs  uh uh uh.pdf', 'b%C3%A4%C3%A4%C3%A4js%20%20uh%20uh%20uh.pdf'],
			['bäääjs  🤌.pdf', 'b%C3%A4%C3%A4%C3%A4js%20%20%F0%9F%A4%8C.pdf'],
		])('should encode urls', async (filename: string, urlName: string) => {
			await StorageObject.get(ctx, 'container', filename)

			expect(head).toBeCalledWith(`/container/${urlName}`)
		})

		it('should return a StorageObject instance', async () => {
			const file = await StorageObject.get(ctx, 'container', 'file.pdf')

			expect(file).toBeInstanceOf(StorageObject)
		})

		it.each([404, 403])('should throw simple error for %p', async (status: number) => {
			expect.hasAssertions()

			head.mockRejectedValueOnce({ response: { status } })

			try {
				await StorageObject.get(ctx, 'container', 'file.pdf')
			} catch (err: any) {
				expect(err.message).toBe('File does not exist or not allowed.')
			}
		})

		it('should retry only once if token expired', async () => {
			expect.hasAssertions()
			const invalidateToken = jest.fn()
			s.invalidateToken = invalidateToken

			head.mockRejectedValue({ response: { status: 401 } })

			try {
				await StorageObject.get(ctx, 'container', 'file.pdf')
			} catch (err: any) {
				expect(err.message).toBe('Not authorized')
				expect(mockGetAxios).toBeCalledTimes(2)
			}
		})

		it('should retry once if token expired', async () => {
			const invalidateToken = jest.fn()
			s.invalidateToken = invalidateToken

			head.mockRejectedValueOnce({ response: { status: 401 } })

			const file = await StorageObject.get(ctx, 'container', 'file.pdf')

			expect(file).toBeInstanceOf(StorageObject)
			expect(head).toBeCalledWith('/container/file.pdf')
			expect(head).toBeCalledTimes(2)
			expect(mockGetAxios).toBeCalledTimes(2)
		})

		it('should throw full errors for other codes', async () => {
			expect.hasAssertions()
			const e = { response: { status: 418 } }
			head.mockRejectedValueOnce(e)

			try {
				await StorageObject.get(ctx, 'container', 'file.pdf')
			} catch (err: any) {
				expect(err).toBe(e)
			}
		})
	})

	describe('Remove', () => {
		it('should get authenticated axios instance', async () => {
			await s.remove()

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to delete the file', async () => {
			await s.remove()

			expect(mockDelete).toBeCalledWith(s.endpoint)
		})
	})

	describe('Download', () => {
		beforeEach(() => {
			jest.clearAllMocks()

			data = {
				bytes: 1337,
				content_type: 'application/pdf',
				hash: 'md5hash',
				last_modified: '2014-01-15T16:37:43.427570',
				name: 'file-name.pdf',
			}

			ctx = {
				authPort: 1234,
				authType: 'password',
				baseUrl: 'www.google.se',
				project: 'my-project',
				credentials: {
					domain: 'default',
					name: 'kalle@anka.se',
					password: 'supersecretpassword',
				},
			}

			get.mockResolvedValue({
				data: [],
				headers: {
					'content-length': '123456',
					'last-modified': '2014-01-15T16:37:43.427570',
					etag: 'somemd5hash',
					'content-type': 'application/pdf',
				},
			})

			s = new StorageObject(ctx, data, 'test-container')
		})

		it('should get authenticated axios instance', async () => {
			await s.download()

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to get the file', async () => {
			await s.download()

			expect(get).toBeCalledWith(s.endpoint, { responseType: 'stream' })
		})

		it('should retry only once if token expired', async () => {
			expect.hasAssertions()
			const invalidateToken = jest.fn()
			s.invalidateToken = invalidateToken

			get.mockRejectedValue({ response: { status: 401 } })

			try {
				await s.download()
			} catch (err: any) {
				expect(err.message).toBe('Not authorized')
				expect(mockGetAxios).toBeCalledTimes(2)
				expect(invalidateToken).toBeCalledTimes(1)
			}
		})

		it('should retry once if token expired', async () => {
			const invalidateToken = jest.fn()
			s.invalidateToken = invalidateToken

			get.mockRejectedValueOnce({ response: { status: 401 } })
			await s.download()

			expect(get).toBeCalledWith(s.endpoint, { responseType: 'stream' })
			expect(mockGetAxios).toBeCalledTimes(2)
			expect(invalidateToken).toBeCalledTimes(1)
		})
	})

	describe('Upload', () => {
		it('should throw if local file does not exist', async () => {
			expect.hasAssertions()

			mockFileExists.mockReturnValueOnce(false)

			try {
				await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf')
			} catch (err: any) {
				expect(err.message).toBe('Path "some/file/path.pdf" not found')
			}
		})

		it('should get authenticated axios instance', async () => {
			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf')

			expect(mockGetAxios).toBeCalled()
		})

		it('should use the correct URL', async () => {
			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf')

			expect(put.mock.calls[0][0]).toBe('/container/file.pdf')
		})

		it.each([
			['file.pdf', 'file.pdf'],
			['file 123.pdf', 'file%20123.pdf'],
			['bäääjs  uh uh uh.pdf', 'b%C3%A4%C3%A4%C3%A4js%20%20uh%20uh%20uh.pdf'],
			['bäääjs  🤌.pdf', 'b%C3%A4%C3%A4%C3%A4js%20%20%F0%9F%A4%8C.pdf'],
		])('should encode urls', async (filename: string, urlName: string) => {
			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', filename)

			expect(put.mock.calls[0][0]).toBe(`/container/${urlName}`)
		})

		it('should add content type to headers', async () => {
			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf')

			expect(put.mock.calls[0][2]).toMatchObject({
				headers: { 'Content-Type': 'application/pdf' },
			})
		})

		it('should upload the file content', async () => {
			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf')

			expect(put.mock.calls[0][1]).toBeInstanceOf(Buffer)
		})

		it('should add custom metadata', async () => {
			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf', {
				metadata: { customData: 'test data' },
			})

			expect(put.mock.calls[0][2]).toEqual({
				headers: {
					'Content-Type': 'application/pdf',
					'X-Object-Meta-customData': 'test-data',
				},
				maxBodyLength: Infinity,
				maxContentLength: Infinity,
				timeout: 20000,
			})
		})

		it('should fetch the file after creation', async () => {
			const mockGet = jest.fn()

			StorageObject.get = mockGet

			await StorageObject.upload(ctx, 'some/file/path.pdf', 'container', 'file.pdf', {
				metadata: { customData: 'test data' },
			})

			expect(mockGet).toBeCalledWith(ctx, 'container', 'file.pdf')
		})
	})

	describe('Move', () => {
		let copy: any, remove: any

		beforeEach(() => {
			copy = jest.fn().mockResolvedValue({ name: 'copied-file.pdf' })
			remove = jest.fn()

			s.remove = remove
			s.copy = copy
		})

		it('should copy the file', async () => {
			await s.move('another-container', 'custom-filename')
			expect(copy).toBeCalledWith('another-container', 'custom-filename', {
				createContainer: false,
			})
		})

		it('should remove the original file', async () => {
			await s.move('another-container', 'custom-filename')

			expect(remove).toBeCalled()
		})

		it('should not remove if copying fails', async () => {
			expect.hasAssertions()

			const e = new Error('this went poopoo')
			copy.mockRejectedValueOnce(e)
			try {
				await s.move('another-container', 'custom-filename')
			} catch (err: any) {
				expect(err).toBe(e)
				expect(remove).not.toBeCalled()
			}
		})
	})
})
