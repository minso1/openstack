import { AuthContext } from '../../types'
import { jsonFormatQuery } from '../../utils'
import { Container } from '../Container'
import { Storage } from '../Storage'

const mockClientConstructor = jest.fn().mockImplementation(function (name, service, type) {
	// @ts-ignore
	this.name = name
	// @ts-ignore
	this.service = service
	// @ts-ignore
	this.type = type
})

const get = jest.fn().mockResolvedValue({
	data: [
		{
			count: 0,
			last_modified: '2014-01-15T16:37:43.427570',
			bytes: 0,
			name: 'demo-container',
		},
		{
			count: 42,
			last_modified: '2014-01-15T16:37:43.427570',
			bytes: 1337,
			name: 'demo-container-with-files',
		},
	],
	headers: {
		'x-container-bytes-used': 1337,
		'last-modified': '2014-01-15T16:37:43.427570',
		'x-container-object-count': 42,
	},
})
const mockAxios = { get }

const mockGetAxios = jest.fn().mockResolvedValue(mockAxios)

jest.mock('../../utils', () => {
	return {
		AuthenticatedClient: class AuthClientMock {
			constructor(ctx: any[], service: any, type: any) {
				return mockClientConstructor.bind(this)(ctx, service, type)
			}

			getAxios(...args: any[]) {
				return mockGetAxios(...args)
			}
		},
	}
})

describe('Storage', () => {
	let s: Storage, ctx: AuthContext

	beforeEach(() => {
		jest.clearAllMocks()

		ctx = {
			authPort: 1234,
			authType: 'password',
			baseUrl: 'www.google.se',
			project: 'my-project',
			credentials: {
				domain: 'default',
				name: 'kalle@anka.se',
				password: 'supersecretpassword',
			},
		}

		s = new Storage(ctx)
	})

	it('has a working constructor', () => {
		expect(s).toBeInstanceOf(Storage)
		expect(mockClientConstructor).toBeCalledWith(ctx, 'object-store', 'public')
	})

	describe('Get Containers', () => {
		it('should create axios instance', async () => {
			await s.getContainers()

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to list all the containers', async () => {
			await s.getContainers()

			expect(get).toBeCalledTimes(1)
			expect(get).toBeCalledWith(jsonFormatQuery)
		})

		it('should create (shallow) Container instances', async () => {
			const resp = await s.getContainers()

			expect(resp.length).toBe(2)
			expect(resp.every((c) => c instanceof Container)).toBeTruthy()
			expect(resp.every((c) => c.content === undefined)).toBeTruthy()
		})

		it('should propagate errors', async () => {
			expect.hasAssertions()

			const e = new Error('this went poopoo')
			get.mockRejectedValueOnce(e)

			try {
				await s.getContainers()
			} catch (err: any) {
				expect(err).toBe(e)
			}
		})
	})

	describe('Get single container', () => {
		beforeEach(() => {
			jest.clearAllMocks()

			ctx = {
				authPort: 1234,
				authType: 'password',
				baseUrl: 'www.google.se',
				project: 'my-project',
				credentials: {
					domain: 'default',
					name: 'kalle@anka.se',
					password: 'supersecretpassword',
				},
			}

			s = new Storage(ctx)
		})

		it('should create axios instance', async () => {
			await s.getContainer('demo-container')

			expect(mockGetAxios).toBeCalledTimes(1)
		})

		it('should call to fetch the container', async () => {
			await s.getContainer('demo-container')

			expect(get).toBeCalledTimes(1)
			expect(get).toBeCalledWith('/demo-container' + jsonFormatQuery)
		})

		it('should create Container instance with file info', async () => {
			get.mockResolvedValueOnce({
				data: [
					{
						hash: 'supercoolhashmd5yo',
						last_modified: '2014-01-15T16:37:43.427570',
						bytes: 42,
						name: 'some-file.pdf',
						content_type: 'application/octet-stream',
					},
					{
						hash: 'supercoolhashmd5yo',
						last_modified: '2014-01-15T16:37:43.427570',
						bytes: 1337,
						name: 'some-other-file.pdf',
						content_type: 'application/pdf',
					},
				],
				headers: {
					'x-container-bytes-used': 1337,
					'last-modified': '2014-01-15T16:37:43.427570',
					'x-container-object-count': 42,
				},
			})

			const resp = await s.getContainer('demo-container')

			expect(resp).toBeInstanceOf(Container)
			expect(resp.content).toBeTruthy()
		})

		it('should throw not found if container does not exist', async () => {
			expect.hasAssertions()

			get.mockRejectedValue({ response: { status: 404 } })

			try {
				await s.getContainer('demo-container')
			} catch (err: any) {
				expect(err.message).toBe('Container does not exist')
			}
		})

		it('should retry once if token expired', async () => {
			expect.hasAssertions()
			const invalidateToken = jest.fn()
			s.invalidateToken = invalidateToken

			get.mockRejectedValue({ response: { status: 401 } })

			try {
				await s.getContainer('demo-container')
			} catch (err: any) {
				expect(err.message).toBe('Not authorized')
				expect(mockGetAxios).toBeCalledTimes(2)
				expect(invalidateToken).toBeCalledTimes(1)
			}
		})

		it('should retry once if token expired', async () => {
			const invalidateToken = jest.fn()
			s.invalidateToken = invalidateToken

			get.mockRejectedValueOnce({ response: { status: 401 } })
			get.mockResolvedValueOnce({
				data: [
					{
						hash: 'supercoolhashmd5yo',
						last_modified: '2014-01-15T16:37:43.427570',
						bytes: 42,
						name: 'some-file.pdf',
						content_type: 'application/octet-stream',
					},
					{
						hash: 'supercoolhashmd5yo',
						last_modified: '2014-01-15T16:37:43.427570',
						bytes: 1337,
						name: 'some-other-file.pdf',
						content_type: 'application/pdf',
					},
				],
				headers: {
					'x-container-bytes-used': 1337,
					'last-modified': '2014-01-15T16:37:43.427570',
					'x-container-object-count': 42,
				},
			})

			const resp = await s.getContainer('demo-container')

			expect(resp).toBeInstanceOf(Container)
			expect(resp.content).toBeTruthy()
		})

		it('should propagate errors other than 404s', async () => {
			expect.hasAssertions()

			get.mockRejectedValue({ response: { status: 418 } })

			try {
				await s.getContainer('demo-container')
			} catch (err: any) {
				expect(err).toMatchObject({ response: { status: 418 } })
			}
		})
	})
})
