/* eslint-disable @typescript-eslint/no-explicit-any */
import { AuthenticatedClient, FileHelper, hyphenate } from '../utils'
import { AuthContext, FileData, RawStorageObject } from '../types'
import { Container } from './Container'
import { AxiosError, AxiosInstance, AxiosStatic } from 'axios'
import { ReadStream } from 'fs'
import { Storage } from './Storage'

export type StorageObjectCreationArgs = {
	/**
	 * Metadata to assign to the file.
	 *
	 * Must be case insensitive
	 */
	metadata: Record<string, string>
	/**
	 * If the container should be created if it does not exist.
	 */
	createContainer: boolean
}

export type StorageObjectHeaders = {
	'content-length': string
	'last-modified': string
	etag: string
	'content-type': string
}

/**
 * Class for a Storage Object, a file, that is stored in a [[Container]] in Swift
 */
export class StorageObject extends AuthenticatedClient {
	/**
	 * MIME type of the file
	 */
	public contentType: string
	/**
	 * Filename
	 */
	public name: string
	public lastModified: Date
	/**
	 * MD5 hash of the file contents
	 */
	public hash: string
	/**
	 * Size of the file in bytes.
	 */
	public bytes: number
	/**
	 * Name of the container where the file is located
	 */
	public containerName: string

	constructor(
		context: AuthContext,
		{ bytes, last_modified, name, content_type, hash }: RawStorageObject,
		containerName: string
	) {
		super(context, 'object-store', 'public')

		this.bytes = bytes
		this.lastModified = new Date(last_modified)

		this.name = name

		this.contentType = content_type

		this.hash = hash

		this.containerName = containerName
	}

	/**
	 * Endpoint to the file. To be appended to the url as
	 * `https://swifturl.com:5000/ACCOUNT/CONTAINERNAME/FILENAME`
	 */
	get endpoint(): string {
		return `/${this.containerName}/${this.name}`
	}

	/**
	 * Fetches the file contents and information about the file type.
	 * @param path Path to the file on disk
	 * @returns The file content as a stream and the content type
	 */
	private static getLocalFile(path: string): FileData {
		if (!FileHelper.fileExists(path)) {
			throw new Error(`Path "${path}" not found`)
		}

		return FileHelper.fileToStream(path)
	}

	/**
	 * Converts metadata to a header for passing to Swift.
	 * @param name The header name
	 * @param val The header value
	 * @returns Key-Value pair with header name and header value
	 */
	private static convertToHeader([name, val]: [string, string]): [string, string] {
		return [`X-Object-Meta-${hyphenate(name)}`, hyphenate(val)]
	}

	/**
	 * Generates headers for a file upload.
	 * @param axios The authenticated axios instance. Used to not override the preset headers
	 * @param contentType The content type to create
	 * @param metadata Optional metadata to assign to the file
	 * @returns The headers to add to the upload request
	 */
	private static getHeaders(
		axios: AxiosStatic | AxiosInstance,
		contentType: string,
		metadata?: Record<string, string>
	): Record<string, string> {
		const headers: any = axios.defaults.headers.common ?? {}

		headers['Content-Type'] = contentType

		if (metadata) {
			// * Convert metadata to supported headers.
			// * This is quite naive and does not check that the strings only contains ASCII-characters.
			Object.entries(metadata)
				.map((kv) => StorageObject.convertToHeader(kv))
				.forEach(([name, value]) => (headers[name] = value))
		}

		return headers
	}

	/**
	 * Parses the headers from the Swift api to be able to construct a StorageObject
	 * @param headers The headers to parse
	 * @param name Name of the file
	 * @returns Raw storage objedt to pass to the [[StorageObject]] constructor
	 */
	public static parseHeaders(headers: StorageObjectHeaders, name: string): RawStorageObject {
		return {
			bytes: parseInt(headers['content-length']),
			content_type: headers['content-type'],
			hash: headers.etag,
			last_modified: headers['last-modified'],
			name,
		}
	}

	/**
	 * Fetches a single file.
	 * @param context Auth contect with credentials etc
	 * @param container The container where the file is located
	 * @param filename Name of the file
	 * @returns Metadata about the file
	 */
	public static async get(
		context: AuthContext,
		container: Container | string,
		filename: string,
		isRetry = false
	): Promise<StorageObject> {
		const axios = await new AuthenticatedClient(context, 'object-store', 'public').getAxios()
		const containerName = typeof container === 'string' ? container : container.name
		const path = `/${containerName}/${filename}`

		try {
			const resp = await axios.head(encodeURI(path))

			const data = StorageObject.parseHeaders(resp.headers as any, filename)

			return new StorageObject(context, data, containerName)
		} catch (err) {
			const status = Number((<AxiosError>err).response?.status)

			if (status === 401) {
				if (!isRetry) {
					return this.get(context, container, filename, true)
				}

				throw new Error('Not authorized')
			}
			if ([404, 403].includes(status)) {
				throw new Error('File does not exist or not allowed.')
			} else {
				throw err
			}
		}
	}

	/**
	 * Uploads a file into a container.
	 * @param context Auth context with credentials etc
	 * @param file Path to the file or a stream/buffer with the file content and the content type
	 * @param container The container or it's name
	 * @param filename Name to give the file in the container. Does *not* have to be the same as the original name
	 * @param args Optional arguments for the file upload such as metadata to be added or if the container should be created if it doesnt exist.
	 * @returns The uploaded file
	 */
	public static async upload(
		context: AuthContext,
		file: string | FileData,
		container: Container | string,
		filename: string,
		args?: Partial<StorageObjectCreationArgs>,
		attempt?: number
	): Promise<StorageObject> {
		const { contentType, data } = typeof file === 'string' ? this.getLocalFile(file) : file

		const axios = await new AuthenticatedClient(context, 'object-store', 'public').getAxios()

		const headers = StorageObject.getHeaders(axios, contentType, args?.metadata)

		const containerName = typeof container === 'string' ? container : container.name
		const path = `/${containerName}/${filename}`

		try {
			await axios.put(encodeURI(path), data, {
				headers,
				maxBodyLength: Infinity,
				maxContentLength: Infinity,
				timeout: 20 * 1000, // 20s timeout
			})

			return StorageObject.get(context, containerName, filename)
		} catch (err: any) {
			const e = err as AxiosError

			// Check for both client side as well as server side timeouts
			const isTimeout = err?.code === 'ECONNABORTED' || e.response?.status === 408

			// * Create container if it does not exist and try again
			if (args?.createContainer && e.response?.status === 404) {
				await Container.create(context, containerName)
				return StorageObject.upload(context, file, container, filename, args)
			} else if (isTimeout && (attempt || 0) < 3) {
				// allow 3 timeouts
				return StorageObject.upload(
					context,
					file,
					container,
					filename,
					args,
					(attempt || 0) + 1
				)
			}

			throw err
		}
	}

	/**
	 * Removes the file
	 */
	public async remove(): Promise<void> {
		const axios = await this.getAxios()

		await axios.delete(this.endpoint)
	}

	/**
	 * Downloads the file as a stream
	 *
	 * @todo TODO: Add options for other response types
	 *
	 * @returns The file content as a `ReadStream`
	 */
	public async download(isRetry = false): Promise<ReadStream> {
		const axios = await this.getAxios()

		try {
			const { data } = await axios.get<ReadStream>(this.endpoint, { responseType: 'stream' })
			return data
		} catch (err) {
			const e = err as AxiosError

			if (e.response?.status === 401) {
				if (!isRetry) {
					this.invalidateToken()
					return this.download(true)
				}

				throw new Error('Not authorized')
			}
			throw err
		}
	}

	/**
	 * Copies a file into another container.
	 *
	 * @example
	 * ```typescript
	 * const myFile = await storage.getFile('my-container', 'demo-file.pdf')
	 * const copiedFile = await myFile.copy(
	 *   'another-container',
	 *   'custom-filename.pdf',
	 *   { createContainer: true } // Create the container if it doesn't exist
	 * )
	 * ```
	 *
	 * @param toContainer The container to copy to
	 * @param filename The filename to use in the target container
	 * @param createContainer If the target container should be created if it doesn't exist.
	 * @returns The freshly copied file
	 */
	public async copy(
		toContainer: string | Container,
		filename = this.name,
		{ createContainer = false } = {}
	): Promise<StorageObject> {
		const to =
			toContainer instanceof Container
				? toContainer
				: await new Storage(this.context).getContainer(toContainer, { createContainer })

		const data = await this.download()
		return to.uploadFile({ contentType: this.contentType, data }, filename)
	}

	/**
	 * Moves a file from one container to another. It first copies then deletes
	 * to avoid data loss.
	 *
	 * @example
	 * ```typescript
	 * const myFile = await storage.getFile('my-container', 'demo-file.pdf')
	 * const movedFile = await myFile.move(
	 *   'another-container',
	 *   'custom-filename.pdf',
	 *   { createContainer: true } // Create the container if it doesn't exist
	 * )
	 *
	 * console.log(movedFile.containerName) // 'another-container'
	 * console.log(movedFile.name) // 'custom-filename.pdf'
	 *
	 * await storage.getFile('my-container', 'demo-file.pdf') // This now throws "not found"
	 * ```
	 * @param toContainer The target container
	 * @param filename The filename to use in the target container
	 * @param createContainer
	 * @returns The copied file
	 */
	public async move(
		toContainer: string | Container,
		filename = this.name,
		{ createContainer = false } = {}
	): Promise<StorageObject> {
		const copiedFile = await this.copy(toContainer, filename, { createContainer })
		await this.remove()
		return copiedFile
	}
}
