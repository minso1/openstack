import { AuthContext, FileData, RawContainer, RawStorageObject } from '../types'
import { AuthenticatedClient, jsonFormatQuery } from '../utils'
import { Container, ContainerCreationArgs } from './Container'
import { AxiosError } from 'axios'
import { StorageObjectCreationArgs, StorageObject } from '.'

/**
 * Wrapper class that has some helper methods for getting and setting data.
 */
export class Storage extends AuthenticatedClient {
	constructor(context: AuthContext) {
		super(context, 'object-store', 'public')
	}

	/**
	 * Fetches all available containers.
	 *
	 * Shallow, no information about the individual objects in the container.
	 * @returns All available containers
	 */
	public async getContainers(): Promise<Container[]> {
		const axios = await this.getAxios()
		const resp = await axios.get<RawContainer[]>(jsonFormatQuery)

		return resp.data.map((c) => new Container(this.context, c))
	}

	/**
	 * Fetches a single storage container.
	 *
	 * Deep, includes information about the individual objects in the container
	 * such as size, content type, etc.
	 * @param containerName The container to fetch
	 * @returns Information about the container
	 */
	public async getContainer(
		containerName: string,
		{ createContainer = false, isRetry = false } = {}
	): Promise<Container> {
		try {
			const axios = await this.getAxios()
			const resp = await axios.get<RawStorageObject[]>(`/${containerName}${jsonFormatQuery}`)

			return new Container(
				this.context,
				Container.parseGetHeaders(resp.headers, containerName),
				resp.data
			)
		} catch (err) {
			const e = err as AxiosError

			// Show a smaller error for common error codes:
			if (e.response?.status === 401) {
				if (!isRetry) {
					this.invalidateToken()
					return this.getContainer(containerName, { createContainer, isRetry: true })
				}

				throw new Error('Not authorized')
			} else if ([404, 403].includes(e.response?.status ?? 0)) {
				if (createContainer) {
					return this.createContainer(containerName)
				} else {
					throw new Error('Container does not exist')
				}
			} else {
				// For all strange errors, give the full axios error
				throw e
			}
		}
	}

	/**
	 * Creates or updates a container.
	 * @param containerName The name of the container to create
	 * @param creationArgs Optional parameters for adding custom metadata and ACL etc.
	 * @returns The container that was created
	 */
	public async createContainer(
		containerName: string,
		creationArgs?: Partial<ContainerCreationArgs>
	): Promise<Container> {
		return Container.create(this.context, containerName, creationArgs)
	}

	public async getFile(container: Container | string, filename: string): Promise<StorageObject> {
		return StorageObject.get(this.context, container, filename)
	}

	public async uploadFile(
		data: string | FileData,
		container: Container | string,
		filename: string,
		args?: Partial<StorageObjectCreationArgs>
	): Promise<StorageObject> {
		return StorageObject.upload(this.context, data, container, filename, args)
	}

	public async removeFile(container: Container | string, filename: string): Promise<void> {
		const file = await StorageObject.get(this.context, container, filename)
		await file.remove()
	}
}
