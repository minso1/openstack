/* eslint-disable @typescript-eslint/no-explicit-any */
import { AxiosError } from 'axios'
import { AuthContext, FileData, RawContainer, RawStorageObject } from '../types'
import { AuthenticatedClient, hyphenate, jsonFormatQuery } from '../utils'
import { StorageObject, StorageObjectCreationArgs } from './StorageObject'

export type FileFilterFunc = (obj: StorageObject, i: number, arr: StorageObject[]) => boolean

export type ContainerCreationArgs = {
	metadata: Record<string, string>
	acl: unknown
}

/**
 * Container that consists of 0 or more [[StorageObject]]
 */
export class Container extends AuthenticatedClient {
	/**
	 * Number of files in the container
	 */
	public count: number
	public lastModified: Date
	/**
	 * Total size of the container
	 */
	public bytes: number
	public name: string
	/**
	 * The content of the container.
	 */
	public content: StorageObject[] | undefined

	constructor(
		context: AuthContext,
		{ bytes, count, last_modified, name }: RawContainer,
		content?: RawStorageObject[]
	) {
		super(context, 'object-store', 'public')

		this.name = name
		this.bytes = bytes
		this.count = count
		this.lastModified = new Date(last_modified)

		if (content) {
			this.assignStorageObjects(content)
		}
	}

	/**
	 * Converts the raw storage data to proper StorageObject and assigns it to the `content` array.
	 * @param content The raw storage object data
	 */
	private assignStorageObjects(content: RawStorageObject[]): void {
		this.content = content?.map((rawObj) => new StorageObject(this.context, rawObj, this.name))
	}

	/**
	 * Converts metadata to a header for passing to Swift.
	 * @param name The header name
	 * @param val The header value
	 * @returns Key-Value pair with header name and header value
	 */
	private static convertToHeader([name, val]: [string, string]): [string, string] {
		return [`X-Container-Meta-${hyphenate(name)}`, hyphenate(val)]
	}

	/**
	 * Uploads a file within the container
	 * @param data Path to the file you want to upload or a buffer-like object with content type provided
	 * @param filename Name of the file you want to write
	 * @param args Additional arguments for uploading
	 * @returns The file created
	 */
	public async uploadFile(
		data: string | FileData,
		filename: string,
		args?: Partial<StorageObjectCreationArgs>
	): Promise<StorageObject> {
		const file = await StorageObject.upload(this.context, data, this, filename, args)
		await this.refresh()

		return file
	}

	/**
	 * Fetches the containers content.
	 * @param forceRefresh if you want to make sure to get fresh data
	 * @returns The containers content
	 */
	public async listFiles({ forceRefresh = false } = {}): Promise<StorageObject[]> {
		if (!this.content || forceRefresh) {
			await this.refresh()
		}

		return this.content ?? []
	}

	/**
	 * Removes file in batch from the container.
	 * @param filterFunc Optional function to pass to filter files arbitrarily.
	 * @returns The modified instance
	 */
	public async removeFiles(filterFunc?: FileFilterFunc): Promise<Container> {
		const filesToRemove = (filterFunc ? this.content?.filter(filterFunc) : this.content) ?? []

		// Extract all the filenames to easier remove from this instance after
		const names = new Set(filesToRemove.map((f) => f.name))

		await Promise.all(filesToRemove.map((f) => f.remove()))

		// Remove the deleted files from this instance & update data
		this.content = (this.content ?? []).filter((f) => !names.has(f.name))
		this.count = this.content.length
		this.bytes = this.content.reduce((sum, file) => (sum += file.bytes), 0)
		this.lastModified = new Date()

		return this
	}

	/**
	 * Removes the container
	 */
	public async remove(): Promise<void> {
		try {
			const axios = await this.getAxios()
			await axios.delete(`/${this.name}`)
		} catch (err) {
			const e = err as AxiosError

			if ([404, 403].includes(Number(e.response?.status))) {
				throw new Error('Container does not exist or not allowed.')
			} else {
				throw err
			}
		}
	}

	/**
	 * Refreshes the containers content & metadata to make sure that the information is fresh af.
	 *
	 * Useful after modfiying content and stuff like that.
	 * @returns The refreshed container
	 */
	public async refresh(): Promise<Container> {
		const axios = await this.getAxios()
		const { data, headers } = await axios.get<RawStorageObject[]>(`/${this.name}${jsonFormatQuery}`)

		const { bytes, count, last_modified } = Container.parseGetHeaders(headers, this.name)

		this.bytes = bytes
		this.count = count
		this.lastModified = new Date(last_modified)

		this.assignStorageObjects(data)

		return this
	}

	/**
	 * When getting a single container the information about the container itself is in the headers.
	 *
	 * This method parsees this into a RawContainer to be able to create a Container instance.
	 *
	 * @todo TODO: Add parsing of metadata headers
	 *
	 * @param headers The headers to extract information from
	 * @param name The container name
	 * @returns The raw container to pass to the constructor
	 */
	public static parseGetHeaders(headers: any, name: string): RawContainer {
		return {
			bytes: headers?.['x-container-bytes-used'],
			last_modified: headers?.['last-modified'],
			count: headers?.['x-container-object-count'],
			name,
		}
	}

	/**
	 * Creates (or updates if it already exists) a container.
	 *
	 * Note that all metadata **must** be case insensitive. Both key and value.
	 *
	 * @see [Openstack Swift Documentation](https://docs.openstack.org/api-ref/object-store/index.html?expanded=show-object-metadata-detail,create-container-detail#create-container)
	 *
	 * @todo TODO: Add better support for ACL
	 *
	 * @param context Auth context
	 * @param containerName The name of the container to create
	 * @param args optional arguments to add metadata, permissions etc.
	 * @returns The container that (probably) was created
	 */
	public static async create(
		context: AuthContext,
		containerName: string,
		args?: Partial<ContainerCreationArgs>
	): Promise<Container> {
		const len = Number(containerName?.length)

		if (!containerName || len > 255 || containerName !== hyphenate(containerName)) {
			throw new Error(
				'Invalid container name. Expected string with length between 1 & 255 chars and no whitespace'
			)
		}

		const client = await new AuthenticatedClient(context, 'object-store', 'public').getAxios()
		const headers = client.defaults.headers.common ?? {}

		if (args?.metadata) {
			// * Convert metadata to supported headers.
			// * This is quite naive and does not check that the strings only contains ASCII-characters.
			Object.entries(args.metadata)
				.map((kv) => Container.convertToHeader(kv))
				.forEach(([name, value]) => (headers[name] = value))
		}

		// TODO: Add ACL support here

		const path = `/${containerName}`

		await client.put(path, {}, { headers })

		// Since the PUT call is idempotent there is a possibility that the container already existed.
		// So we get it and return the instance.
		const raw = await client.get<RawStorageObject[]>(path + jsonFormatQuery)

		return new Container(context, Container.parseGetHeaders(raw.headers, containerName), raw.data)
	}
}
