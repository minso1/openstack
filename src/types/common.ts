import { ReadStream } from 'fs'

/**
 * The different kind of services that are listed in the service catalog
 */
export type OpenStackService =
	| 'identity'
	| 'cloudformation'
	| 'volumev2'
	| 'metering'
	| 'placement'
	| 'volume'
	| 'load-balancer'
	| 'orchestration'
	| 'object-store'
	| 'key-manager'
	| 'rating'
	| 'compute'
	| 'network'
	| 'registration'
	| 'volumev3'
	| 'image'
	| 'metric'

/**
 * Openstak interface types. Used to select URLs
 */
export type InterfaceType = 'internal' | 'public' | 'admin'

export type RawEndpoint = {
	url: string
	interface: InterfaceType
	region: string
	region_id: string
	id: string
}

export type RawContainer = {
	count: number
	last_modified: string
	bytes: number
	name: string
}

export type RawStorageObject = {
	hash: string
	last_modified: string
	bytes: number
	name: string
	content_type: string
}

export type RawCatalog = {
	endpoints: RawEndpoint[]
	type: OpenStackService
	id: string
	name: string
}

export type FileData = {
	contentType: string
	data: ReadStream | Buffer
}

export interface AuthContext {
	baseUrl: string
	endpoint?: string
	authPort: number | string
	authType: AuthType
	credentials: Credential
	project: string
}

export interface AuthResponse {
	headers: { 'x-subject-token': string }
	data: { token: { is_domain: boolean; expires_at: string; catalog: RawCatalog[] } }
}

export type Credential = UserAuth | ApplicationAuth

export type AuthType = 'password' | 'application_credential'

export interface UserAuth {
	domain: string
	name: string
	password: string
}

export interface ApplicationAuth {
	id: string
	secret: string
}
