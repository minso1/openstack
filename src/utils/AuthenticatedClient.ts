import { AxiosStatic, AxiosInstance } from 'axios'
import { Token, Identity } from '../identity'
import { createAxios } from '.'
import { AuthContext, InterfaceType, OpenStackService } from '../types'

/**
 * Helper class to create an authenticated client to be used as parent
 * for more service oriented classes.
 */
export class AuthenticatedClient {
	public service: OpenStackService
	public type: InterfaceType
	#context: AuthContext
	#token: Token | undefined
	#axios: AxiosStatic | AxiosInstance | undefined
	#identity: Identity | undefined

	constructor(ctx: AuthContext, service: OpenStackService, type: InterfaceType) {
		this.service = service
		this.type = type
		this.#context = ctx
	}

	protected get context(): AuthContext {
		return this.#context
	}

	protected set context(ctx: AuthContext) {
		this.#context = ctx
	}

	public invalidateToken(): void {
		this.#token = undefined
	}

	/**
	 * Creates an authenticated client with baseUrl set.
	 * @returns Axios instance with baseURL and auth headers set
	 */
	public async getAxios(): Promise<AxiosStatic | AxiosInstance> {
		const dependencies = {
			axios: this.#axios,
			context: this.context,
			identity: this.#identity,
			token: this.#token,
		}

		const { axios, identity, token } = await createAxios(dependencies, this.service, this.type)

		this.#axios = axios
		this.#identity = identity
		this.#token = token

		return this.#axios
	}
}
