import axios, { AxiosStatic, AxiosInstance } from 'axios'
import { Token, Identity } from '../identity'
import { AuthContext, OpenStackService, InterfaceType } from '../types'

export type CreateAxiosInput = {
	context: AuthContext
	token: Token | undefined
	axios: AxiosStatic | AxiosInstance | undefined
	identity: Identity | undefined
}

export type CreateAxiosOutput = {
	token: Token
	axios: AxiosStatic | AxiosInstance
	identity: Identity
}

/**
 * Creates an authenticated axios client. To be used in [[AuthenticatedClient]]
 * @param input The input to use
 * @param service The service to generate token for
 * @param type The type of interface to use
 * @returns Authenticated client and [[Identity]] instance
 */
export const createAxios = async (
	input: CreateAxiosInput,
	service: OpenStackService,
	type: InterfaceType
): Promise<CreateAxiosOutput> => {
	const output = {
		axios: input.axios,
		identity: input.identity || new Identity(input.context),
	} as CreateAxiosOutput

	if (!input.token || input.token?.shouldRefresh || !output.axios) {
		const token = await output.identity.getToken()

		// * input is fetched by `identity.getToken` above so to avoid
		// * 2 api calls we await again instead of Promise.all.
		// * So input looks like waterfalling but it isn't.
		const catalog = await output.identity.getServiceCatalog()

		output.token = token
		output.axios = axios.create({
			headers: { 'X-Auth-Token': token.token },
			baseURL: catalog[service][type],
			maxBodyLength: Infinity,
			maxContentLength: Infinity,
		})
	}

	return output
}

export const jsonFormatQuery = '?format=json'