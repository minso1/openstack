import { existsSync, createReadStream } from 'fs'
import mime from 'mime-types'
import { FileData } from '../types'

export class FileHelper {
	static fileExists(path: string): boolean {
		return existsSync(path)
	}

	static fileToStream(path: string): FileData {
		const contentType = mime.lookup(path) || 'application/octet-stream'
		return { contentType, data: createReadStream(path) }
	}
}
