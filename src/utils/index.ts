export * from './AuthenticatedClient'
export * from './AxiosUtils'
export * from './FileHelper'

/**
 * Replaces all whitespace with hyphens.
 * @param str String to hyphenate
 * @returns String where all whitespace are hyphens instead
 */
export const hyphenate = (str: string): string => str.replace(/\s+/g, '-').toLowerCase()
